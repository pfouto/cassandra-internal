package org.pfouto.internal.connection;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandlerAdapter;
import io.netty.channel.ChannelPromise;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pfouto.internal.messaging.*;
import org.pfouto.internal.utils.CompactEndpointSerializationHelper;
import org.pfouto.internal.utils.Pair;

import java.net.InetAddress;
import java.util.List;
import java.util.Map;

public class MessageEncoder extends ChannelOutboundHandlerAdapter {

    private static final Logger logger = LogManager.getLogger(MessageEncoder.class);


    @Override
    public void write(ChannelHandlerContext ctx, Object o, ChannelPromise promise) {
        ByteBuf out = null;
        try {
            if (!(o instanceof InternalMessage || o instanceof DownMessage ||
                    o instanceof ClockMessage || o instanceof StatusMessage ||
                    o instanceof MigrateMessage || o instanceof FullClockMessage)) {
                logger.error("Unsupported object received in encoder: " + o);
                return;
            }

            //logger.debug("Encoding: " + o + " " + ctx.channel() + " " + System.identityHashCode(this));

            out = ctx.alloc().buffer();
            ByteBufOutputStream outStream = new ByteBufOutputStream(out);
            out.writerIndex(4);
            Message m = (Message) o;

            //code
            outStream.writeInt(m.getCode());
            //from
            CompactEndpointSerializationHelper.serialize(m.getFrom(), outStream);
            //verb
            outStream.writeInt(m.getVerb());
            //timestamp
            outStream.writeLong(m.getTimestamp());
            //specific
            if (o instanceof InternalMessage) {
                InternalMessage im = (InternalMessage) o;
                Map<String, List<Pair<Integer, InetAddress>>> targets = im.getTargets();
                Map<String, Integer> clock = im.getDepClock();
                //Targets
                outStream.writeInt(targets.size());
                for (Map.Entry<String, List<Pair<Integer, InetAddress>>> entry : targets.entrySet()) {
                    outStream.writeUTF(entry.getKey());
                    outStream.writeInt(entry.getValue().size());
                    for (Pair<Integer, InetAddress> pair : entry.getValue()) {
                        outStream.writeInt(pair.left);
                        CompactEndpointSerializationHelper.serialize(pair.right, outStream);
                    }
                }
                //Clock
                outStream.writeInt(clock.size());
                for (Map.Entry<String, Integer> clockEntry : clock.entrySet()) {
                    outStream.writeUTF(clockEntry.getKey());
                    outStream.writeInt(clockEntry.getValue());
                }
                //SourceDC
                outStream.writeUTF(im.getSourceDc());
                outStream.writeInt(im.getMutationId());
                outStream.writeInt(im.getSaturnLabel());
            } else if (o instanceof FullClockMessage) {
                FullClockMessage fcm = (FullClockMessage) o;
                Map<String, Integer> clock = fcm.getClock();
                //Clock
                outStream.writeInt(clock.size());
                for (Map.Entry<String, Integer> clockEntry : clock.entrySet()) {
                    outStream.writeUTF(clockEntry.getKey());
                    outStream.writeInt(clockEntry.getValue());
                }
                //SourceDC
                outStream.writeUTF(fcm.getSourceDc());
            } else if (o instanceof DownMessage) { //Down
                DownMessage dm = (DownMessage) o;
                outStream.writeInt(dm.getId());
            } else if (o instanceof ClockMessage) {
                ClockMessage cm = (ClockMessage) o;
                outStream.writeUTF(cm.getClock().left);
                outStream.writeInt(cm.getClock().right);
            } else if (o instanceof StatusMessage){
                StatusMessage sm = (StatusMessage) o;
                outStream.writeUTF(sm.getDataCenter());

                Map<String, Pair<Integer, Long>> status = sm.getStatus();
                outStream.writeInt(status.size());
                for (Map.Entry<String, Pair<Integer, Long>> entry : status.entrySet()) {
                    outStream.writeUTF(entry.getKey());
                    outStream.writeInt(entry.getValue().left);
                    outStream.writeLong(entry.getValue().right);
                }
            } else if (o instanceof MigrateMessage){
                MigrateMessage mm = (MigrateMessage) o;
                outStream.writeLong(mm.getThread());
                outStream.writeUTF(mm.getSourceDc());
                outStream.writeInt(mm.getPossibleDatacenters().size());
                for(String dc : mm.getPossibleDatacenters()){
                    outStream.writeUTF(dc);
                }
                //Clock
                Map<String, Integer> clock = mm.getClock();
                outStream.writeInt(clock.size());
                for (Map.Entry<String, Integer> clockEntry : clock.entrySet()) {
                    outStream.writeUTF(clockEntry.getKey());
                    outStream.writeInt(clockEntry.getValue());
                }
                //outStream.writeInt(mm.getLabelSaturn());
                //CompactEndpointSerializationHelper.serialize(mm.getSrcSaturn(), outStream);
            }

            //write size
            int index = out.writerIndex();
            out.writerIndex(0);
            out.writeInt(index - 4);
            out.writerIndex(index);
            ctx.write(out, promise);
        } catch (Exception e) {
            if (out != null && out.refCnt() > 0)
                out.release(out.refCnt());
            promise.tryFailure(e);
            logger.error("Exception: " + e.getMessage());
            e.printStackTrace();
        }
    }

}

