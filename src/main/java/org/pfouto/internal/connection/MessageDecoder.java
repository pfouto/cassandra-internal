package org.pfouto.internal.connection;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pfouto.internal.messaging.*;
import org.pfouto.internal.utils.CompactEndpointSerializationHelper;
import org.pfouto.internal.utils.Pair;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.util.*;

public class MessageDecoder extends ByteToMessageDecoder { // (1)

    private static final Logger logger = LogManager.getLogger(MessageDecoder.class);

    private int size = -1;

    @Override
    public void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) {

        ByteBufInputStream inputStream = new ByteBufInputStream(in);
        try {

            if (size == -1 && in.readableBytes() >= 4) {
                size = in.readInt();
            }

            if (size != -1 && in.readableBytes() >= size) {
                //code
                int messageCode = in.readInt();
                //from
                InetAddress from = CompactEndpointSerializationHelper.deserialize(inputStream);
                //verb
                int verb = in.readInt();
                //timestamp
                long timestamp = in.readLong();
                //specific stuff
                int parameterCount;
                int label;
                Map<String, List<Pair<Integer, InetAddress>>> targets;
                switch (messageCode) {
                    case UpMessage.CODE:
                        //targets
                        parameterCount = in.readInt();
                        targets = new HashMap<>();
                        readMap(inputStream, parameterCount, targets);
                        label = in.readInt();
                        //create obj
                        out.add(new UpMessage(targets, label, from, verb, timestamp));
                        break;
                    case InternalMessage.CODE:
                        //targets
                        parameterCount = in.readInt();
                        targets = new HashMap<>();
                        readMap(inputStream, parameterCount, targets);
                        //clock
                        int clockSize = in.readInt();
                        Map<String, Integer> clock = new HashMap<>();
                        for(int i = 0;i<clockSize;i++){
                            String key = inputStream.readUTF();
                            int val = inputStream.readInt();
                            clock.put(key, val);
                        }
                        //sourceDC
                        String sourceDc = inputStream.readUTF();
                        //mutationId
                        int mutationId = inputStream.readInt();
                        label = in.readInt();
                        //create obj
                        out.add(new InternalMessage(targets, sourceDc, mutationId, clock, label, from, verb, timestamp));
                        break;
                    case FullClockMessage.CODE:
                        //clock
                        int cS = in.readInt();
                        Map<String, Integer> cl = new HashMap<>();
                        for(int i = 0;i<cS;i++){
                            String key = inputStream.readUTF();
                            int val = inputStream.readInt();
                            cl.put(key, val);
                        }
                        //sourceDC
                        String sDC = inputStream.readUTF();
                        //create obj
                        out.add(new FullClockMessage(sDC, cl, from, verb, timestamp));
                        break;
                    case ClockMessage.CODE:
                        //clock
                        String left = inputStream.readUTF();
                        int right = inputStream.readInt();
                        out.add(new ClockMessage(Pair.create(left, right), from, verb, timestamp));
                        break;
                    case AckMessage.CODE:
                        int id = in.readInt();
                        InetAddress node = CompactEndpointSerializationHelper.deserialize(inputStream);
                        out.add(new AckMessage(id, node, from, verb, timestamp));
                        break;
                    case StatusMessage.CODE:
                        String dataCenter = inputStream.readUTF();
                        int statusSize = in.readInt();
                        Map<String, Pair<Integer, Long>> status = new HashMap<>();
                        for(int i = 0; i<statusSize;i++){
                            String key = inputStream.readUTF();
                            int val = inputStream.readInt();
                            long ts = inputStream.readLong();
                            status.put(key, Pair.create(val, ts));
                        }
                        out.add(new StatusMessage(timestamp, dataCenter, status));
                        break;
                    case MigrateMessage.CODE:
                        long thread = inputStream.readLong();
                        String sdc = inputStream.readUTF();
                        int listSize = in.readInt();
                        List<String> possibleDcs = new ArrayList<>(listSize);
                        for(int i = 0;i<listSize;i++){
                            possibleDcs.add(inputStream.readUTF());
                        }
                        //clock
                        int cSize = in.readInt();
                        Map<String, Integer> c = new HashMap<>();
                        for(int i = 0;i<cSize;i++){
                            String key = inputStream.readUTF();
                            int val = inputStream.readInt();
                            c.put(key, val);
                        }
                        //label = in.readInt();
                        //InetAddress srcSat = CompactEndpointSerializationHelper.deserialize(inputStream);
                        out.add(new MigrateMessage(thread, sdc, possibleDcs, -1, null, c, from, verb, timestamp));
                        break;
                    default:
                        throw new Exception("unknown/unhandled messageType: " + messageCode);
                }
                size = -1;
            }

        } catch (Exception e) {
            logger.error("Error Decoding message: " + e.getMessage());
            e.printStackTrace();
            System.exit(1);
        }
    }

    private void readMap(ByteBufInputStream inputStream, int parameterCount, Map<String, List<Pair<Integer, InetAddress>>> targets) throws IOException {
        if (parameterCount > 0) {
            for (int i = 0; i < parameterCount; i++) {
                String key = inputStream.readUTF();
                int valueSize = inputStream.readInt();
                List<Pair<Integer, InetAddress>> value = new ArrayList<>();
                for (int j = 0; j < valueSize; j++) {
                    int id = inputStream.readInt();
                    InetAddress addr = CompactEndpointSerializationHelper.deserialize(inputStream);
                    value.add(Pair.create(id, addr));
                }
                targets.put(key, value);
            }
        }
    }
}
