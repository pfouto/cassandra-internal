package org.pfouto.internal.connection;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pfouto.internal.main.Main;
import org.pfouto.internal.messaging.*;
import org.pfouto.internal.state.*;
import org.pfouto.internal.state.saturn.MessageExecutorSaturnV4;
import org.pfouto.internal.state.saturn.MessageReceiverSaturnV4;
import org.pfouto.internal.state.v4.MessageExecutorV4;
import org.pfouto.internal.state.v4.MessageReceiverV4;
import org.pfouto.internal.tree.TreeManager;

import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.concurrent.atomic.AtomicInteger;

class ConnectionHandler extends ChannelInboundHandlerAdapter {

    private static final Logger logger = LogManager.getLogger(ConnectionHandler.class);

    private String internalDc;

    @Override
    public void channelRegistered(final ChannelHandlerContext ctx) {

    }

    @Override
    public void channelActive(final ChannelHandlerContext ctx) throws UnknownHostException { // (1)
        if (Main.MODE == Main.MODE_V3 || Main.MODE == Main.MODE_V4 || Main.MODE == Main.MODE_SATURN) {
            internalDc = TreeManager.getInstance().getDcNameByInternalAddress(((InetSocketAddress) ctx.channel().remoteAddress()).getAddress());
            if (internalDc != null && internalDc.equals(TreeManager.getInstance().getLocalDc())) {
                internalDc = null;
            }

            if (internalDc != null) {
                logger.debug("Channel to internal " + internalDc + " active " + ctx.channel().remoteAddress());
            } else {
                logger.debug("Channel to client " + ctx.channel().remoteAddress() + " active");
            }
        }

    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {

        if (Main.MODE == Main.MODE_V1) {
            MessageHandler.messageInQueue.add((Message) msg);

        } else if (Main.MODE == Main.MODE_V2) {
            MessageReceiverV2.messageInQueue.add((Message) msg);

        } else if (Main.MODE == Main.MODE_V3) {

            if (internalDc != null && internalDc.equals(TreeManager.getInstance().getLocalDc())) {
                logger.error("Message from myself (!?)");
                logger.error(msg);
                System.exit(0);
            }

            if (internalDc != null) {
                if (msg instanceof StatusMessage) {
                    MessageReceiverV3.addToNoDependencyQueue((Message) msg);
                } else {
                    MessageExecutorV3.addToWaitingMessages((Message) msg);
                }
            } else {
                MessageReceiverV3.addToNoDependencyQueue((Message) msg);
            }
        } else if (Main.MODE == Main.MODE_V4) {

            if (internalDc != null && internalDc.equals(TreeManager.getInstance().getLocalDc())) {
                logger.error("Message from myself (!?)");
                logger.error(msg);
                System.exit(0);
            }

            if (internalDc == null || msg instanceof StatusMessage || msg instanceof ClockMessage) {
                MessageReceiverV4.addToLocalQueue((Message) msg);
            } else {
                MessageExecutorV4.addToWaitingMessages((Message) msg);
            }

        } else if (Main.MODE == Main.MODE_SATURN) {
            if (internalDc != null && internalDc.equals(TreeManager.getInstance().getLocalDc())) {
                logger.error("Message from myself (!?)");
                logger.error(msg);
                System.exit(0);
            }

            if (internalDc != null) {
                if (msg instanceof StatusMessage) {
                    MessageReceiverSaturnV4.addToLocalQueue((Message) msg);
                } else {
                    MessageExecutorSaturnV4.propagateAndMaybeKeep((Message) msg);
                }
            } else {
                MessageReceiverSaturnV4.addToLocalQueue((Message) msg);
            }

        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        logger.error("Exception in channel " + ctx.channel().remoteAddress() + ":\n" + cause);
        ctx.close();
    }

}
