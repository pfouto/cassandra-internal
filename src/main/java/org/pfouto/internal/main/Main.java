package org.pfouto.internal.main;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pfouto.internal.connection.ConnectionManager;
import org.pfouto.internal.state.*;
import org.pfouto.internal.state.saturn.MessageExecutorSaturnV4;
import org.pfouto.internal.state.saturn.MessageReceiverSaturnV4;
import org.pfouto.internal.state.v4.MessageExecutorV4;
import org.pfouto.internal.state.v4.MessageReceiverV4;
import org.pfouto.internal.tree.TreeManager;

import java.io.IOException;

public class Main {

    private static final Logger logger = LogManager.getLogger(Main.class);

    public static int MODE;

    public final static int MODE_SATURN = 0;
    public final static int MODE_V1 = 1;
    public final static int MODE_V2 = 2;
    public final static int MODE_V3 = 3;
    public final static int MODE_V4 = 4;

    public static void main(String[] args) throws IOException, InterruptedException {

        TreeManager.getInstance().loadLayout(args[0]);

        MODE = Integer.valueOf(args[1]);

        assert MODE >= 0 && MODE <= 4;

        ConnectionManager.init();

        if (MODE == MODE_V1) {
            MessageHandler mHandler = new MessageHandler();
            mHandler.mainLoop();

        }  else if (MODE == MODE_V2){
            MessageReceiverV2 mReceiver = new MessageReceiverV2();
            MessageExecutorV2 mExecutor = new MessageExecutorV2();
            new Thread(mReceiver).start();
            new Thread(mExecutor).start();

        } else if (MODE == MODE_SATURN) {
            MessageReceiverSaturnV4 mReceiver = new MessageReceiverSaturnV4();
            MessageExecutorSaturnV4 mExecutor = new MessageExecutorSaturnV4();
            new Thread(mReceiver, "Rec").start();
            new Thread(mExecutor, "Exe").start();
        } else if (MODE == MODE_V3){
            MessageReceiverV3 mReceiver = new MessageReceiverV3();
            MessageExecutorV3 mExecutor = new MessageExecutorV3();
            new Thread(mReceiver, "Rec").start();
            new Thread(mExecutor, "Exe").start();
        } else if (MODE == MODE_V4){
            MessageReceiverV4 mReceiver = new MessageReceiverV4();
            MessageExecutorV4 mExecutor = new MessageExecutorV4();
            new Thread(mReceiver, "Rec").start();
            new Thread(mExecutor, "Exe").start();
        }
    }
}
