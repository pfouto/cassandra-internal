package org.pfouto.internal.messaging;

import org.pfouto.internal.utils.Pair;

import java.net.InetAddress;

public class ClockMessage extends Message{

    public final static int CODE = 3;

    private Pair<String, Integer> clock;

    public ClockMessage(Pair<String, Integer> clock, InetAddress from, int verb, long timestamp) {
        super(from, verb, CODE, timestamp);
        this.clock = clock;
    }

    public Pair<String, Integer> getClock() {
        return clock;
    }

    public static ClockMessage fromInternalWithMutationId(InternalMessage m){
        String sourceDc = m.getSourceDc();
        Pair<String, Integer> pair = Pair.create(sourceDc, m.getMutationId());
        return new ClockMessage(pair, m.getFrom(), m.getVerb(), m.getTimestamp());
    }
    public static ClockMessage fromInternal(InternalMessage m){
        String sourceDc = m.getSourceDc();
        Pair<String, Integer> pair = Pair.create(sourceDc, m.getDepClock().get(sourceDc));
        return new ClockMessage(pair, m.getFrom(), m.getVerb(), m.getTimestamp());
    }

    @Override
    public String toString(){
        return super.toString() + " CLOCK: " + clock;
    }
}