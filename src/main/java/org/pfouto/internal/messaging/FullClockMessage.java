package org.pfouto.internal.messaging;

import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

public class FullClockMessage extends Message{

    public final static int CODE = 7;

    String sourceDc;
    private Map<String, Integer> clock;

    public FullClockMessage(String sourceDc, Map<String, Integer> clock, InetAddress from, int verb, long timestamp) {
        super(from, verb, CODE, timestamp);
        this.sourceDc = sourceDc;
        this.clock = clock;
    }

    public Map<String, Integer> getClock() {
        return clock;
    }

    public static FullClockMessage fromInternal(InternalMessage m){
        return new FullClockMessage(m.getSourceDc(), new HashMap<>(m.getDepClock()),
                m.getFrom(), m.getVerb(), m.getTimestamp());
    }

    @Override
    public String toString(){
        return super.toString() + " CLOCK: " + clock + " SOURCEDC: " + sourceDc;
    }

    public String getSourceDc() {
        return sourceDc;
    }
}