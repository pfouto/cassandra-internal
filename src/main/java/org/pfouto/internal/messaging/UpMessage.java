package org.pfouto.internal.messaging;

import org.pfouto.internal.utils.Pair;

import java.net.InetAddress;
import java.util.List;
import java.util.Map;

public class UpMessage extends Message
{

    public static final int CODE = 0;

    //<DC <messageId, target>>
    private final Map<String, List<Pair<Integer, InetAddress>>> targets;

    private int saturnLabel;

    public UpMessage(Map<String, List<Pair<Integer, InetAddress>>> targets,
                     InetAddress from, int verb, long timestamp)
    {
        this(targets, -1, from, verb, timestamp);
    }
    public UpMessage(Map<String, List<Pair<Integer, InetAddress>>> targets, int saturnLabel,
                     InetAddress from, int verb, long timestamp)
    {
        super(from, verb, CODE, timestamp);
        this.targets = targets;
        this.saturnLabel = saturnLabel;
    }

    public Map<String, List<Pair<Integer, InetAddress>>> getTargets()
    {
        return targets;
    }

    public int getSaturnLabel() {
        return saturnLabel;
    }

    @Override
    public String toString(){
        return super.toString() + " TARGETS: " + targets.entrySet() + " LBL: " + saturnLabel;
    }

}