package org.pfouto.internal.messaging;

import org.pfouto.internal.utils.Pair;

import java.util.Map;

public class StatusMessage extends Message{

    public static final int CODE = 5;

    private String dataCenter;
    private Map<String, Pair<Integer, Long>> status;

    public StatusMessage(long timestamp, String dataCenter, Map<String, Pair<Integer, Long>> status) {
        super(null, -1, CODE, timestamp);
        this.status = status;
        this.dataCenter = dataCenter;
    }

    public Map<String, Pair<Integer, Long>> getStatus() {
        return status;
    }

    public String getDataCenter() {
        return dataCenter;
    }

    public String toString(){
        return super.toString() + " dc: " + dataCenter + " status: " + status;
    }

}
