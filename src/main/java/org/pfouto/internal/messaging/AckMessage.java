package org.pfouto.internal.messaging;

import java.net.InetAddress;

public class AckMessage extends Message{

    public final static int CODE = 4;

    private final int id;
    private final InetAddress node;

    public AckMessage(int messageId, InetAddress node, InetAddress from, int verb, long timestamp) {
        super(from, verb, CODE, timestamp);
        this.id = messageId;
        this.node = node;
    }

    public int getId() {
        return id;
    }

    public InetAddress getNode() {
        return node;
    }

    @Override
    public String toString(){
        return super.toString() + " ID: " + id + " NODE: " + node;
    }
}