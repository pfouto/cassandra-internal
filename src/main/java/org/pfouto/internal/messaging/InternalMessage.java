package org.pfouto.internal.messaging;

import org.pfouto.internal.utils.Pair;

import java.net.InetAddress;
import java.util.*;

public class InternalMessage extends Message {

    public static final int CODE = 1;

    //<DC <Id, target>>
    private Map<String, List<Pair<Integer, InetAddress>>> targets;
    private Map<String, Integer> depClock;
    private String sourceDc;
    private int saturnLabel;
    private int mutationId;

    public InternalMessage(Map<String, List<Pair<Integer, InetAddress>>> targets,
                           String sourceDc, int mutationId, Map<String, Integer> depClock,
                           InetAddress from, int verb, long timestamp) {
        this(targets, sourceDc, mutationId, depClock, -1, from, verb,timestamp);
    }


    public InternalMessage(Map<String, List<Pair<Integer, InetAddress>>> targets,
                           String sourceDc, int mutationId, Map<String, Integer> depClock, int saturnLabel,
                           InetAddress from, int verb, long timestamp) {
        super(from, verb, CODE, timestamp);
        this.targets = targets;
        this.depClock = depClock;
        if(this.depClock == null){
            this.depClock = Collections.emptyMap();
        }
        this.sourceDc = sourceDc;
        this.saturnLabel = saturnLabel;
        this.mutationId = mutationId;
    }

    public InternalMessage filterAndClone(Set<String> filter){
        Map<String, List<Pair<Integer, InetAddress>>> newTargets = new HashMap<>();
        for(String dc: filter){
            List<Pair<Integer, InetAddress>> pairs = targets.get(dc);
            if(pairs != null)
                newTargets.put(dc, pairs);
        }
        return new InternalMessage(newTargets, sourceDc, mutationId, new HashMap<>(depClock), saturnLabel, getFrom(), getVerb(), getTimestamp());
    }

    public InternalMessage filterAndClone(String dc){
        Map<String, List<Pair<Integer, InetAddress>>> newTargets = new HashMap<>();
        newTargets.put(dc, targets.get(dc));
        return new InternalMessage(newTargets, sourceDc, mutationId, depClock, saturnLabel, getFrom(), getVerb(), getTimestamp());
    }

    public int getMutationId() {
        return mutationId;
    }

    public Map<String, List<Pair<Integer, InetAddress>>> getTargets() {
        return targets;
    }

    public int getSaturnLabel() {
        return saturnLabel;
    }

    public Map<String, Integer> getDepClock() {
        return depClock;
    }

    public String getSourceDc() {
        return sourceDc;
    }

    @Override
    public String toString(){
        return super.toString() + " TARGETS: " + targets.entrySet() + " DEPCLOCK: " + depClock + " ID " + mutationId + " SOURCEDC: " + sourceDc + " LBL: " + saturnLabel;
    }
}