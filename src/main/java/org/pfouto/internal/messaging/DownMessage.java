package org.pfouto.internal.messaging;

import java.net.InetAddress;

public class DownMessage extends Message{

    public final static int CODE = 2;

    private final int id;

    public DownMessage(int messageId, InetAddress from, int verb, long timestamp) {
        super(from, verb, CODE, timestamp);
        this.id = messageId;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString(){
        return super.toString() + " ID: " + id;
    }
}