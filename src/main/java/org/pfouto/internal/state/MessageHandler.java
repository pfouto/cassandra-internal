package org.pfouto.internal.state;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pfouto.internal.connection.ConnectionManager;
import org.pfouto.internal.messaging.*;
import org.pfouto.internal.tree.TreeManager;
import org.pfouto.internal.utils.Pair;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class MessageHandler {

    private static final Logger logger = LogManager.getLogger(MessageHandler.class);

    public static BlockingQueue<Message> messageInQueue = new LinkedBlockingQueue<>();

    //          dc                          clock      message
    private Map<String, PriorityQueue<Pair<Integer, Message>>> waitingMessages;
    private Set<String> updatedClocks;

    //          from
    private Map<InetAddress, WaitingState> currentMutations;

    private int mutationClock;
    private Map<String, Integer> clock;

    private long lastStatus;
    private static final long STATUS_INTERVAL = 1000;

    private Map<String, Pair<Integer, Long>> internalsStatus;
    private Comparator<Pair<Integer, Message>> comparator;


    //Logging
    private int inSinceLastStatus;
    private int waitingSinceLastStatus;

    private int totalMessages;
    private int totalAck;
    private int totalInternal;
    private int totalClock;
    private int totalUp;
    private int totalStatus;
    private int totalMigrate;
    private int storedMessages;

    public MessageHandler() {

        comparator = Comparator.comparing(t0 -> t0.left);

        mutationClock = 0;
        clock = new HashMap<>();
        waitingMessages = new HashMap<>();
        currentMutations = new HashMap<>();
        updatedClocks = new HashSet<>();
        internalsStatus = new HashMap<>();

        lastStatus = 0;


        inSinceLastStatus = 0;
        waitingSinceLastStatus = 0;
        totalMessages = 0;
        totalUp = 0;
        totalClock = 0;
        totalInternal = 0;
        totalAck = 0;
        totalMessages = 0;
        totalStatus = 0;
        storedMessages = 0;
    }

    public void mainLoop() throws InterruptedException {
        //noinspection InfiniteLoopStatement
        while (true) {

            long now = System.currentTimeMillis();
            if (now - lastStatus > STATUS_INTERVAL) {

                lastStatus = now;
                int inQueueSize = messageInQueue.size();
                int waitingSize = waitingMessages.values().stream().mapToInt(PriorityQueue::size).sum();


                //propagate info
                propagateSaturationState(inQueueSize, waitingSize);


                //Logging
                String status = "\nQueueSize:\tI " + inQueueSize + "\tW " + waitingSize
                        + "\nExecutedOps:\tI " + inSinceLastStatus + "\tI->W " + storedMessages + "\tW " + waitingSinceLastStatus

                        + "\nReceivedOps:\tU." + totalUp + "\tI." + totalInternal
                        + "\tC." + totalClock + "\tA." + totalAck
                        + "\tM." + totalMigrate
                        + "\tT." + totalMessages;

                if (inQueueSize > 20000 || waitingSize > 20000) {
                    logger.warn(status);
                } else {
                    logger.info(status);
                }
                logger.info(clock);
                //logger.info(internalsStatus);

                inSinceLastStatus = 0;
                waitingSinceLastStatus = 0;
                storedMessages = 0;
            }

            assert updatedClocks.isEmpty();

            Message poll = messageInQueue.take();
            handleMessage(poll);
            totalMessages++;
            inSinceLastStatus++;

            while (!updatedClocks.isEmpty()) {
                Iterator<String> it = updatedClocks.iterator();
                String clock = it.next();
                it.remove();
                checkWaitingMessages(clock);
            }
        }
    }

    private void propagateSaturationState(int inQueueSize, int waitingSize) {
        TreeManager tree = TreeManager.getInstance();

        long time = System.currentTimeMillis();
        internalsStatus.put(tree.getLocalDc(),
                Pair.create(inQueueSize + waitingSize, time));

        StatusMessage sm = new StatusMessage(time, tree.getLocalDc(), new HashMap<>(internalsStatus));

        List<String> targets = new LinkedList<>();
        targets.addAll(tree.getDownBranches(tree.getMainTree()));
        String inverse = tree.getUpBranch(tree.getMainTree());
        if (inverse != null)
            targets.add(inverse);

        for (String dc : targets) {
            try {
                InetAddress dcAddress = InetAddress.getByName(tree.getDcAddress(dc));
                ConnectionManager.getConnectionToHigh(dcAddress).writeAndFlush(sm);
            } catch (Exception e) {
                logger.error("Failed to send status message to " + dc + ". " + e.getMessage());
            }
        }

    }

    private void checkWaitingMessages(String clockPos) {
        logger.debug("Checking waiting: " + clockPos);
        int clockVal = clock.get(clockPos);
        PriorityQueue<Pair<Integer, Message>> pairs = waitingMessages.get(clockPos);
        if (pairs == null) return;
        while (!pairs.isEmpty() && pairs.peek().left <= clockVal) {
            Message m = pairs.poll().right;
            waitingSinceLastStatus++;
            logger.debug("Found: " + m);
            if (m instanceof InternalMessage) {
                tryExecuteInternalMessage((InternalMessage) m);
            } else if (m instanceof ClockMessage) {
                executeClockMessage((ClockMessage) m);
            } else if (m instanceof MigrateMessage){
                tryExecuteMigrateMessage((MigrateMessage) m);
            }
        }
    }

    private void handleMessage(Message msg) {

        if (msg.getCode() == UpMessage.CODE) {
            UpMessage m = (UpMessage) msg;
            totalUp++;
            logger.debug("Up message received: " + m);
            handleUpMessage(m);
        } else if (msg.getCode() == InternalMessage.CODE) {
            InternalMessage m = (InternalMessage) msg;
            totalInternal++;
            logger.debug("Internal message received: " + m);
            handleInternalMessage(m);
        } else if (msg.getCode() == ClockMessage.CODE) {
            ClockMessage m = (ClockMessage) msg;
            totalClock++;
            logger.debug("Clock message received " + m);
            handleClockMessage(m);
        } else if (msg.getCode() == AckMessage.CODE) {
            totalAck++;
            AckMessage m = (AckMessage) msg;
            logger.debug("Ack message received " + m);
            handleAckMessage(m);
        } else if (msg.getCode() == StatusMessage.CODE) {
            StatusMessage m = (StatusMessage) msg;
            totalStatus++;
            logger.debug("Status message received " + m);
            handleStatusMessage(m);
        } else if (msg.getCode() == MigrateMessage.CODE) {
            MigrateMessage m = (MigrateMessage) msg;
            totalMigrate++;
            logger.debug("Migrate message received " + m);
            handleMigrateMessage(m);
        } else {
            logger.error("Unknown message received: " + msg);
        }
    }

    private void handleMigrateMessage(MigrateMessage m) {
        TreeManager tree = TreeManager.getInstance();

        Map<String, Integer> opClock = m.getClock();
        List<String> possibleDcs = m.getPossibleDatacenters();

        //if from client
        if (possibleDcs.size() > 1) {
            assert m.getSourceDc().equals(tree.getLocalDc());
            assert opClock == null || opClock.isEmpty();
            String bestDc = null;
            int bestDcLoad = Integer.MAX_VALUE;
            for (String dc : possibleDcs) {
                Pair<Integer, Long> pair = internalsStatus.get(dc);
                if (pair == null)
                    continue;
                int load = pair.left;
                if (load < bestDcLoad) {
                    bestDcLoad = load;
                    bestDc = dc;
                }
            }
            possibleDcs = new LinkedList<>();
            possibleDcs.add(bestDc);
            opClock = cloneMap(clock);
        }

        MigrateMessage newMsg = new MigrateMessage(m.getThread(), m.getSourceDc(), possibleDcs,
                opClock, m.getFrom(), m.getVerb(), m.getTimestamp());
        assert newMsg.getPossibleDatacenters().size() == 1;

        if(newMsg.getPossibleDatacenters().get(0).equals(tree.getLocalDc())){
            //target is me
            tryExecuteMigrateMessage(newMsg);
        } else {
            //or not
            for (String dc : tree.getDownBranches(newMsg.getSourceDc())) {
                Set<String> targets = tree.getTargetsBehindBranch(newMsg.getSourceDc(), dc);
                if(targets.contains(newMsg.getPossibleDatacenters().get(0))){
                    logger.debug("Redirecting migrate to: " + dc);
                    try {
                        InetAddress dcAddress = InetAddress.getByName(tree.getDcAddress(dc));
                        ConnectionManager.getConnectionToHigh(dcAddress).writeAndFlush(newMsg);
                    } catch (Exception e) {
                        logger.error("Failed to migration message to " + dc + ". " + e.getMessage());
                    }
                    break;
                }
            }
        }
    }

    private void tryExecuteMigrateMessage(MigrateMessage msg) {
        //execute or store in queue (another) queue
        Pair<String, Integer> missingDep =
                checkMissingDependencies(msg.getClock(), msg.getSourceDc(), msg.getVerb() == Message.VERB_WRITE);

        if (missingDep == null) {
            executeMigrateMessage(msg);
        } else {
            waitingMessages.computeIfAbsent(missingDep.left, k -> new PriorityQueue<>(comparator))
                    .add(Pair.create(missingDep.right, msg));
            storedMessages++;
        }
    }

    private void executeMigrateMessage(MigrateMessage msg) {
        TreeManager tree = TreeManager.getInstance();
        assert msg.getPossibleDatacenters().get(0).equals(tree.getLocalDc());
        logger.debug("Redirecting migrate to client: " + msg.getFrom());
        try {
            ConnectionManager.getConnectionToClient(msg.getFrom()).writeAndFlush(msg);
        } catch (Exception e) {
            logger.error("Failed to migration message to client " + msg.getFrom() + ". " + e.getMessage());
        }
    }

    private void handleStatusMessage(StatusMessage sm) {
        for (Map.Entry<String, Pair<Integer, Long>> entry : sm.getStatus().entrySet()) {
            String dc = entry.getKey();
            Pair<Integer, Long> remotePair = entry.getValue();
            Pair<Integer, Long> localPair = internalsStatus.get(dc);
            if (localPair == null || remotePair.right > localPair.right) {
                internalsStatus.put(dc, remotePair);
            }
        }
    }

    private void handleClockMessage(ClockMessage msg) {
        assert !msg.getClock().left.equals(TreeManager.getInstance().getLocalDc());
        //propagate
        for (String dc : TreeManager.getInstance().getDownBranches(msg.getClock().left)) {
            try {
                InetAddress dcAddress = InetAddress.getByName(TreeManager.getInstance().getDcAddress(dc));
                ConnectionManager.getConnectionToHigh(dcAddress).writeAndFlush(msg);
            } catch (UnknownHostException | InterruptedException e) {
                logger.error("Failed to forward clock to " + dc + ". " + e.getMessage());
            }
        }
        // (try to) execute
        if (canExecuteClock(msg)) {
            executeClockMessage(msg);
        } else {
            waitingMessages.computeIfAbsent(msg.getClock().left, k -> new PriorityQueue<>(comparator))
                    .add(Pair.create(msg.getClock().right - 1, msg));
            storedMessages++;
        }
    }

    private void executeClockMessage(ClockMessage msg) {
        updateClock(msg.getClock().left, msg.getClock().right);
    }

    private void handleUpMessage(UpMessage msg) {
        String localDc = TreeManager.getInstance().getLocalDc();

        Map<String, Integer> opClock = cloneMap(clock);

        if (msg.getVerb() == Message.VERB_WRITE) {
            mutationClock++;
            opClock.put(localDc, mutationClock);
            logger.debug("New mutation clock: " + mutationClock);
        }

        InternalMessage internalMessage =
                new InternalMessage(msg.getTargets(), localDc, 0 , opClock, msg.getFrom(), msg.getVerb(), msg.getTimestamp());
        handleInternalMessage(internalMessage);
    }

    private void handleInternalMessage(InternalMessage msg) {
        TreeManager tree = TreeManager.getInstance();
        ClockMessage cm = null;

        //propagate
        for (String dc : tree.getDownBranches(msg.getSourceDc())) {

            //Find targets in that branch
            Set<String> targets = tree.getTargetsBehindBranch(msg.getSourceDc(), dc);

            targets.retainAll(msg.getTargets().keySet());

            //Decide message or clock (or none) and create
            Message m = null;
            if (!targets.isEmpty()) {
                m = msg.filterAndClone(targets);
                logger.debug("Redirecting to: " + dc);
            } else if (msg.getVerb() == Message.VERB_WRITE) {
                if (cm == null) cm = ClockMessage.fromInternal(msg);
                logger.debug("Clock to: " + dc);
                m = cm;
            }

            //Send
            if (m != null) {
                try {
                    InetAddress dcAddress = InetAddress.getByName(tree.getDcAddress(dc));
                    ConnectionManager.getConnectionToHigh(dcAddress).writeAndFlush(m);
                } catch (Exception e) {
                    logger.error("Failed to forward message to " + dc + ". " + e.getMessage());
                }
            }
        }

        //if read not for me, nothing to do here
        if ((msg.getVerb() == Message.VERB_READ_REQUEST || msg.getVerb() == Message.VERB_READ_REPLY)
                && !msg.getTargets().containsKey(tree.getLocalDc()))
            return;

        //if read of me or mutate, execute
        tryExecuteInternalMessage(msg);
    }

    private void tryExecuteInternalMessage(InternalMessage msg) {
        //execute or store in queue (another) queue
        Pair<String, Integer> missingDep =
                checkMissingDependencies(msg.getDepClock(), msg.getSourceDc(), msg.getVerb() == Message.VERB_WRITE);

        if (missingDep == null) {
            executeInternalMessage(msg);
        } else {
            waitingMessages.computeIfAbsent(missingDep.left, k -> new PriorityQueue<>(comparator))
                    .add(Pair.create(missingDep.right, msg));
            storedMessages++;
        }
    }

    private void executeInternalMessage(InternalMessage msg) {
        String localDc = TreeManager.getInstance().getLocalDc();

        if (msg.getVerb() == Message.VERB_READ_REQUEST || msg.getVerb() == Message.VERB_READ_REPLY) {
            assert msg.getTargets().containsKey(localDc);
            sendToLocalNodes(msg);
        } else if (msg.getVerb() == Message.VERB_WRITE) {
            //execute, either send to local nodes, or act as clock
            if (msg.getTargets().containsKey(localDc)) {
                WaitingState put = currentMutations.put(msg.getFrom(),
                        new WaitingState(msg, localDc));
                assert put == null;
                sendToLocalNodes(msg);
            } else {
                updateClock(msg.getSourceDc(), msg.getDepClock().get(msg.getSourceDc()));
            }
        } else {
            assert false;
        }
    }

    private void handleAckMessage(AckMessage msg) {
        WaitingState waitingState = currentMutations.get(msg.getFrom());
        if (waitingState != null) {
            waitingState.receiveAck(msg.getNode(), msg.getId());
            if (waitingState.isCompleted()) {
                logger.debug("Mutation completed");
                currentMutations.remove(msg.getFrom());
                updateClock(waitingState.getSourceDc(), waitingState.getClock().get(waitingState.getSourceDc()));
            }
        }
    }

    private void sendToLocalNodes(InternalMessage msg) {

        logger.debug("Sending to local nodes:" + msg.getDepClock());
        //send to nodes
        List<Pair<Integer, InetAddress>> localNodes = msg.getTargets().get(TreeManager.getInstance().getLocalDc());

        assert localNodes != null && !localNodes.isEmpty();
        for (Pair<Integer, InetAddress> p : localNodes) {
            DownMessage down = new DownMessage(p.left, msg.getFrom(), msg.getVerb(), msg.getTimestamp());
            try {
                ConnectionManager.getConnectionToLow(p.right).writeAndFlush(down);
            } catch (Exception e) {
                logger.error("Failed to forward message to " + p.right + ". " + e.getMessage());
            }
        }
    }

    //Returns null if can execute, or a missing clock if not
    private Pair<String, Integer> checkMissingDependencies(Map<String, Integer> messageClock,
                                                           String sourceDc, boolean isMutation) {

        int maxDiff = 0;
        int val = 0;
        String dc = null;

        for (Map.Entry<String, Integer> e : messageClock.entrySet()) {
            int opClock = e.getValue();
            clock.putIfAbsent(e.getKey(), 0);
            int myClock = clock.get(e.getKey());

            int diff = opClock - myClock;

            if (e.getKey().equals(sourceDc) && diff > (isMutation ? 1 : 0) && diff > maxDiff) {
                dc = e.getKey();
                maxDiff = diff;
                val = opClock - (isMutation ? 1 : 0);
            }
            if (!e.getKey().equals(sourceDc) && diff > 0 && diff > maxDiff) {
                dc = e.getKey();
                maxDiff = diff;
                val = opClock;
            }
        }
        if (dc == null) {
            return null;
        } else {
            return Pair.create(dc, val);
        }
    }

    private Pair<String, Integer> checkMissingDependenciesOld(Map<String, Integer> messageClock,
                                                              String sourceDc, boolean isMutation) {
        for (Map.Entry<String, Integer> e : messageClock.entrySet()) {
            int opClock = e.getValue();
            clock.putIfAbsent(e.getKey(), 0);
            int myClock = clock.get(e.getKey());

            if (e.getKey().equals(sourceDc) && opClock > myClock + (isMutation ? 1 : 0)) {
                return Pair.create(e.getKey(), opClock - (isMutation ? 1 : 0));
            }
            if (!e.getKey().equals(sourceDc) && opClock > myClock) {
                return Pair.create(e.getKey(), opClock);
            }
        }
        return null;
    }

    private boolean canExecuteClock(ClockMessage msg) {
        return msg.getClock().right == clock.getOrDefault(msg.getClock().left, 0) + 1;
    }

    private void updateClock(String sourceDc, int val) {
        int current = clock.getOrDefault(sourceDc, 0);
        assert val == current + 1;
        if (current < val) {
            clock.put(sourceDc, val);
            logger.debug("New clock: " + clock);
            updatedClocks.add(sourceDc);
        } else {
            assert false;
        }
    }

    private Map<String, Integer> cloneMap(Map<String, Integer> map) {
        return new HashMap<>(map);
    }
}
