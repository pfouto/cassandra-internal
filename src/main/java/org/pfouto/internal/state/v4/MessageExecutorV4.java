package org.pfouto.internal.state.v4;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pfouto.internal.connection.ConnectionManager;
import org.pfouto.internal.messaging.*;
import org.pfouto.internal.state.WaitingState;
import org.pfouto.internal.tree.TreeManager;
import org.pfouto.internal.utils.Pair;

import java.net.InetAddress;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.*;

public class MessageExecutorV4 implements Runnable {

    private static final Logger logger = LogManager.getLogger(MessageExecutorV4.class);

    //                  sourceDc      message
    static Map<String, Queue<Message>> waitingMessages = new ConcurrentHashMap<>();

    private static Object dummyObj = new Object();
    private static BlockingQueue<Object> newMessageQueue = new LinkedBlockingQueue<>();

    //                          from        mId
    static Map<Pair<InetAddress, Integer>, WaitingState> currentMutations = new ConcurrentHashMap<>();

    public MessageExecutorV4() {
    }

    private void mainLoop() throws InterruptedException {

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            for (Map.Entry<String, Queue<Message>> e : waitingMessages.entrySet()) {
                String k = e.getKey();
                Queue<Message> v = e.getValue();
                logger.info(k + ":" + v.size());
                int messages = 10;
                while (!v.isEmpty() && messages > 0) {
                    messages--;
                    Message msg = v.poll();
                    assert msg != null;
                    if (msg.getCode() == InternalMessage.CODE) {
                        InternalMessage m = (InternalMessage) msg;
                        logger.info(m.getSourceDc() + m.getDepClock() + m.getVerb());
                    } else if (msg.getCode() == ClockMessage.CODE) {
                        ClockMessage m = (ClockMessage) msg;
                        logger.info(m.getClock() + "" + m.getVerb());
                    } else if (msg.getCode() == MigrateMessage.CODE) {
                        MigrateMessage m = (MigrateMessage) msg;
                        logger.info(m.getSourceDc() + m.getClock() + m.getVerb());
                    } else {
                        logger.error("Unknown message in final print: " + msg);
                    }
                }
            }

            logger.info("Waiting muts:");
            for (Map.Entry<Pair<InetAddress, Integer>, WaitingState> e : currentMutations.entrySet()) {
                logger.info(e.getKey() + " " + e.getValue().getMessage() + " " + e.getValue().getNodesMap());
            }
            LogManager.shutdown();
        }));

        //String localDc = TreeManager.getInstance().getLocalDc();
        //Queue<Message> localQueue = waitingMessages.computeIfAbsent(localDc, k -> new ConcurrentLinkedQueue<>());

        //noinspection InfiniteLoopStatement
        while (true) {
            newMessageQueue.poll(50, TimeUnit.MILLISECONDS);
            boolean anyExecuted;
            do {
                anyExecuted = false;
                for (Map.Entry<String, Queue<Message>> entry : waitingMessages.entrySet()) {
                    boolean executed;
                    Queue<Message> dcQueue = entry.getValue();
                    //boolean isLocalDcQueue = (dcQueue == localQueue);
                    do {
                        executed = false;
                        Message peek = dcQueue.peek();
                        if (peek != null && (/*isLocalDcQueue ||*/ canExecuteMessage(peek))) {
                            dcQueue.remove();
                            executeMessage(peek);
                            anyExecuted = true;
                            executed = true;
                            MessageReceiverV4.internalMessagesExecuted++;
                        }
                    } while (executed);
                }
            } while (anyExecuted);
            MessageReceiverV4.executorLoops++;
        }
    }

    private static boolean canExecuteMessage(Message msg) {
        if (msg.getCode() == InternalMessage.CODE) {
            InternalMessage m = (InternalMessage) msg;
            return checkMessageDependencies(m.getDepClock());
        } else if (msg.getCode() == MigrateMessage.CODE) {
            MigrateMessage m = (MigrateMessage) msg;
            return checkMessageDependencies(m.getClock());
        } else {
            logger.error("Unknown message received 'canExecute': " + msg);
            System.exit(1);
            return false;
        }
    }

    private static boolean checkMessageDependencies(Map<String, Integer> messageClock) {
        for (Map.Entry<String, Integer> e : messageClock.entrySet()) {

            if (e.getValue() > MessageReceiverV4.executedClock.getOrDefault(e.getKey(), 0)) {
                return false;
            }
        }
        return true;
    }

    private void executeMessage(Message msg) {
        logger.debug("Executing Msg: " + msg);
        if (msg.getCode() == InternalMessage.CODE) {
            executeInternalMessage((InternalMessage) msg);
        } else if (msg.getCode() == MigrateMessage.CODE) {
            executeMigrateMessage((MigrateMessage) msg);
        } else {
            logger.error("Unknown message received in executor: " + msg);
        }
    }

    private void executeMigrateMessage(MigrateMessage msg) {
        TreeManager tree = TreeManager.getInstance();
        assert msg.getPossibleDatacenters().get(0).equals(tree.getLocalDc());

        logger.debug("Redirecting migrate to client: " + msg.getFrom());
        try {
            ConnectionManager.getConnectionToClient(msg.getFrom()).writeAndFlush(msg);
        } catch (Exception e) {
            logger.error("Failed to migration message to client " + msg.getFrom() + ". " + e.getMessage());
        }
    }

    private void executeInternalMessage(InternalMessage msg) {
        String localDc = TreeManager.getInstance().getLocalDc();

        assert msg.getTargets().containsKey(localDc);

        WaitingState waitingState = new WaitingState(msg, localDc);
        for (Pair<InetAddress, Integer> key : waitingState.getMapKeys()) {
            WaitingState put = currentMutations.put(key, waitingState);
            assert put == null;
        }
        MessageReceiverV4.incExecutingClock(msg.getSourceDc(), msg.getMutationId());
        sendToLocalNodes(msg);
    }

    private void sendToLocalNodes(InternalMessage msg) {

        String localDc = TreeManager.getInstance().getLocalDc();

        logger.debug("Sending to local nodes:" + msg.getDepClock());
        for (Pair<Integer, InetAddress> p : msg.getTargets().get(localDc)) {
            DownMessage down = new DownMessage(p.left, msg.getFrom(), msg.getVerb(), msg.getTimestamp());
            try {
                logger.debug("Sending to: " + p.right + " - " + down);
                ConnectionManager.getConnectionToLow(p.right).writeAndFlush(down);
            } catch (Exception e) {
                logger.error("Failed to forward message to " + p.right + ". " + e.getMessage());
            }
        }
    }

    static int getWaitingMessagesSize() {
        return waitingMessages.values().stream().mapToInt(Queue::size).sum();
    }

    public static void addToWaitingMessages(Message msg) {
        MessageReceiverV4.internalMessagesReceived++;
        String sourceDc = null;
        if (msg.getCode() == InternalMessage.CODE) {
            InternalMessage m = (InternalMessage) msg;
            sourceDc = m.getSourceDc();
        } else if (msg.getCode() == ClockMessage.CODE) {
            ClockMessage m = (ClockMessage) msg;
            sourceDc = m.getClock().left;
        } else if (msg.getCode() == MigrateMessage.CODE) {
            MigrateMessage m = (MigrateMessage) msg;
            sourceDc = m.getSourceDc();
        } else {
            logger.error("Unknown message received in addToWaitingMessages: " + msg);
            System.exit(1);
        }
        waitingMessages.computeIfAbsent(sourceDc, k -> new ConcurrentLinkedQueue<>()).add(msg);

        logger.debug("New message from " + sourceDc + ": " + msg);
        signalLoop();
    }

    static void signalLoop() {
        newMessageQueue.add(dummyObj);
    }

    @Override
    public void run() {
        try {
            mainLoop();
        } catch (Exception e) {
            logger.error("Exception in executor thread: " + e);
            System.exit(1);
        }
    }
}
