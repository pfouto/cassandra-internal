package org.pfouto.internal.state.v4;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pfouto.internal.connection.ConnectionManager;
import org.pfouto.internal.messaging.*;
import org.pfouto.internal.state.WaitingState;
import org.pfouto.internal.tree.TreeManager;
import org.pfouto.internal.utils.Pair;

import java.net.InetAddress;
import java.util.*;
import java.util.concurrent.*;

public class MessageReceiverV4 implements Runnable {

    private static final Logger logger = LogManager.getLogger(MessageReceiverV4.class);

    private static BlockingQueue<Message> localQueue = new LinkedBlockingQueue<>();

    static final Map<String, Integer> executedClock = new ConcurrentHashMap<>();
    static final Map<String, Integer> executingClock = new ConcurrentHashMap<>();

    private static Map<String, Queue<Integer>> aheadExecutedClocks = new HashMap<>();

    private static int opCounter = 0;

    private long lastStatus;
    private static final long STATUS_INTERVAL = 3000;

    private Map<String, Pair<Integer, Long>> internalsStatus = new HashMap<>();

    //logs
    static int executorLoops = 0;
    int clientMessagesExecuted = 0;
    static int internalMessagesExecuted = 0;
    int mutationsCompleted = 0;
    int totalMutations = 0;
    static int totalIntExec = 0;
    int maxCurrentMutationSize = 0;

    static int clientMessagesReceived = 0;
    static int internalMessagesReceived = 0;

    public MessageReceiverV4() {
        lastStatus = System.currentTimeMillis();
    }

    private void mainLoop() throws InterruptedException {
        logger.info("RECv4 STARTING");
        //noinspection InfiniteLoopStatement
        while (true) {

            Message in = localQueue.poll(500, TimeUnit.MILLISECONDS);
            if (in != null) {
                handleNoDepMessage(in);
                clientMessagesExecuted++;
            }

            long now = System.currentTimeMillis();
            if (now - lastStatus > STATUS_INTERVAL) {
                int inQueueSize = localQueue.size();
                int waitingSize = MessageExecutorV4.getWaitingMessagesSize();
                //propagate info
                propagateSaturationState(inQueueSize, waitingSize);
                outputLogs(inQueueSize, waitingSize);
                lastStatus = now;
            }
        }
    }

    private void outputLogs(int inQueueSize, int waitingSize) {
        totalIntExec += internalMessagesExecuted;
        //LOG stuff
        StringBuilder sb = new StringBuilder();
        int currentMutationSize = MessageExecutorV4.currentMutations.size();
        if (currentMutationSize > maxCurrentMutationSize) {
            maxCurrentMutationSize = currentMutationSize;
        }

        sb.append("\nInQ: ").append(inQueueSize)
                .append("\tWaitQ: ").append(waitingSize)
                .append("\tExecMutSize: ").append(currentMutationSize).append("(").append(maxCurrentMutationSize).append(")");
        sb.append("\nInRec: ").append(clientMessagesReceived).append("\tInExec: ").append(clientMessagesExecuted)
                .append("\nIntRec: ").append(internalMessagesReceived).append("\tIntExec: ").append(internalMessagesExecuted)
                .append("(").append(totalIntExec).append(")")
                .append("\tLoops: ").append(executorLoops).append("\tMutComplet: ").append(mutationsCompleted).append("(")
                .append(totalMutations).append(")");
        sb.append("\n").append("executedclock: ").append(executedClock.toString());
        sb.append("\n").append("executingclock: ").append(executingClock.toString());

        sb.append("\n");
        MessageExecutorV4.waitingMessages.forEach((k, v) ->
                sb.append(k).append(": ").append(v.size()).append("\t"));

        if (inQueueSize > 100000 || waitingSize > 100000) {
            logger.error(sb.toString());
        } else if (inQueueSize > 20000 || waitingSize > 20000) {
            logger.warn(sb.toString());
        } else {
            logger.info(sb.toString());
        }

        internalMessagesReceived = 0;
        clientMessagesReceived = 0;

        executorLoops = 0;
        clientMessagesExecuted = 0;
        internalMessagesExecuted = 0;
        mutationsCompleted = 0;
    }

    private void handleNoDepMessage(Message msg) {
        if (msg.getCode() != StatusMessage.CODE) {
            logger.debug("Message Received: " + msg);
        }
        if (msg.getCode() == UpMessage.CODE) {
            handleUpMessage((UpMessage) msg);
        } else if (msg.getCode() == AckMessage.CODE) {
            handleAckMessage((AckMessage) msg);
        } else if (msg.getCode() == StatusMessage.CODE) {
            handleStatusMessage((StatusMessage) msg);
        } else if (msg.getCode() == MigrateMessage.CODE) {
            handleMigrateMessage((MigrateMessage) msg);
        } else if (msg.getCode() == ClockMessage.CODE) {
            handleClockMessage((ClockMessage) msg);
        } else {
            logger.error("Unknown message received in receiver: " + msg);
        }
    }

    private void handleUpMessage(UpMessage msg) {
        String localDc = TreeManager.getInstance().getLocalDc();
        int mutationId = ++opCounter;

        Map<String, Integer> depClock = cloneMap(executingClock);
        mergeClocks(depClock, executedClock);
        InternalMessage internalMessage =
                new InternalMessage(msg.getTargets(), localDc, mutationId, depClock, msg.getFrom(), msg.getVerb(), msg.getTimestamp());
        propagateInternalMessage(internalMessage);
        if (msg.getTargets().containsKey(localDc)) {
            MessageExecutorV4.addToWaitingMessages(internalMessage);
        } else {
            handleClockMessage(ClockMessage.fromInternalWithMutationId(internalMessage));
        }
    }

    private void mergeClocks(Map<String,Integer> depClock, Map<String,Integer> executedClock) {
        for(Map.Entry<String, Integer> entry : executedClock.entrySet()){
            depClock.merge(entry.getKey(), entry.getValue(), Math::max);
        }
    }

    private void handleMigrateMessage(MigrateMessage m) {
        String bestDc = null;
        int bestDcLoad = Integer.MAX_VALUE;
        for (String dc : m.getPossibleDatacenters()) {
            Pair<Integer, Long> pair = internalsStatus.get(dc);
            if (pair == null)
                continue;
            int load = pair.left;
            if (load < bestDcLoad) {
                bestDcLoad = load;
                bestDc = dc;
            }
        }
        assert bestDc != null;

        m.getPossibleDatacenters().clear();
        m.getPossibleDatacenters().add(bestDc);

        m.setClock(cloneMap(executingClock));

        //propagate
        try {
            InetAddress dcAddress = InetAddress.getByName(TreeManager.getInstance().getDcAddress(bestDc));
            ConnectionManager.getConnectionToHigh(dcAddress).writeAndFlush(m);
        } catch (Exception e) {
            logger.error("Failed to migration message to " + bestDc + ". " + e.getMessage());
        }
    }

    private void handleStatusMessage(StatusMessage sm) {
        for (Map.Entry<String, Pair<Integer, Long>> entry : sm.getStatus().entrySet()) {
            String dc = entry.getKey();
            Pair<Integer, Long> remotePair = entry.getValue();
            Pair<Integer, Long> localPair = internalsStatus.get(dc);
            if (localPair == null || remotePair.right > localPair.right) {
                internalsStatus.put(dc, remotePair);
            }
        }
    }

    private void handleClockMessage(ClockMessage msg) {
        Pair<String, Integer> clock = msg.getClock();
        incExecutedClock(clock.left, clock.right);
    }

    private void handleAckMessage(AckMessage msg) {
        Pair p = Pair.create(msg.getFrom(), msg.getId());
        WaitingState waitingState = MessageExecutorV4.currentMutations.remove(p);
        if (waitingState != null) {
            waitingState.receiveAck(msg.getNode(), msg.getId());
            if (waitingState.isCompleted()) {
                String sourceDc = waitingState.getSourceDc();
                logger.debug("Mutation completed " + sourceDc + " " + waitingState.getClock().get(sourceDc));
                incExecutedClock(sourceDc, waitingState.getMessage().getMutationId());
                mutationsCompleted++;
                totalMutations++;
            }
        }
    }

    private void propagateInternalMessage(InternalMessage msg) {
        TreeManager tree = TreeManager.getInstance();
        ClockMessage cm = null;

        //propagate
        for (String dc : tree.getDownBranches(msg.getSourceDc())) {
            //Decide message or clock (or none) and create
            Message m = null;
            if (msg.getTargets().get(dc) != null) {
                m = msg.filterAndClone(dc);
                logger.debug("Redirecting to: " + dc);
            } else if (msg.getVerb() == Message.VERB_WRITE) {
                if (cm == null) cm = ClockMessage.fromInternalWithMutationId(msg);
                logger.debug("Clock to: " + dc + " " + cm);
                m = cm;
            }

            //Send
            if (m != null) {
                try {
                    InetAddress dcAddress = InetAddress.getByName(tree.getDcAddress(dc));
                    ConnectionManager.getConnectionToHigh(dcAddress).writeAndFlush(m);
                } catch (Exception e) {
                    logger.error("Failed to forward message to " + dc + ". " + e.getMessage());
                }
            }
        }
    }

    private void incExecutedClock(String sourceDc, int mutationId) {
        int oldClock = executedClock.getOrDefault(sourceDc, 0);
        Queue<Integer> aheadClocks = aheadExecutedClocks.computeIfAbsent(sourceDc, k -> new PriorityQueue<>());

        if (mutationId != oldClock + 1) {
            assert mutationId > oldClock + 1;
            aheadClocks.add(mutationId);
        } else {

            int currentClock = mutationId;
            while (!aheadClocks.isEmpty() && aheadClocks.peek() == currentClock + 1) {
                currentClock = aheadClocks.remove();
            }
            executedClock.put(sourceDc, currentClock);
            logger.debug("New executed clock: " + executedClock);
            MessageExecutorV4.signalLoop();
        }
    }

    static void incExecutingClock(String sourceDc, int mutationId) {
        Integer oldClock = executingClock.put(sourceDc, mutationId);
        assert oldClock==null || mutationId > oldClock;
        logger.debug("New executing clock: " + executedClock);
    }

    private Map<String, Integer> cloneMap(Map<String, Integer> map) {
        return new HashMap<>(map);
    }

    private void propagateSaturationState(int inQueueSize, int waitingSize) {
        TreeManager tree = TreeManager.getInstance();

        long time = System.currentTimeMillis();
        internalsStatus.put(tree.getLocalDc(), Pair.create(inQueueSize + waitingSize, time));
        StatusMessage sm = new StatusMessage(time, tree.getLocalDc(), new HashMap<>(internalsStatus));

        List<String> targets = new LinkedList<>();
        targets.addAll(tree.getDownBranches(tree.getMainTree()));
        String inverse = tree.getUpBranch(tree.getMainTree());
        if (inverse != null)
            targets.add(inverse);

        for (String dc : targets) {
            try {
                InetAddress dcAddress = InetAddress.getByName(tree.getDcAddress(dc));
                ConnectionManager.getConnectionToHigh(dcAddress).writeAndFlush(sm);
            } catch (Exception e) {
                logger.error("Failed to send status message to " + dc + ". " + e.getMessage());
            }
        }
    }

    public static void addToLocalQueue(Message msg) {
        clientMessagesReceived++;
        localQueue.add(msg);
    }

    @Override
    public void run() {
        try {
            mainLoop();
        } catch (Exception e) {
            logger.error("Exception in receiver thread: " + e);
            e.printStackTrace();
            System.exit(1);
        }
    }

}
