package org.pfouto.internal.state;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pfouto.internal.connection.ConnectionManager;
import org.pfouto.internal.messaging.*;
import org.pfouto.internal.tree.TreeManager;
import org.pfouto.internal.utils.Pair;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;
import java.util.concurrent.*;

public class MessageExecutorV3 implements Runnable {

    private static final Logger logger = LogManager.getLogger(MessageExecutorV3.class);

    //                  sourceDc      message
    protected static Map<String, Queue<Message>> waitingMessages;

    private static Object dummyObj = new Object();
    private static BlockingQueue<Object> newMessageQueue;

    //                      from
    protected static Map<InetAddress, WaitingState> currentMutations;

    public MessageExecutorV3() {
        waitingMessages = new ConcurrentHashMap<>();
        currentMutations = new ConcurrentHashMap<>();
        newMessageQueue = new LinkedBlockingQueue<>();
    }

    private void mainLoop() throws InterruptedException {
        //noinspection InfiniteLoopStatement

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            for (Map.Entry<String, Queue<Message>> e : waitingMessages.entrySet()) {
                String k = e.getKey();
                Queue<Message> v = e.getValue();
                logger.info(k + ":" + v.size());
                int messages = 10;
                while (!v.isEmpty() && messages > 0) {
                    messages--;
                    Message msg = v.poll();
                    if (msg.getCode() == InternalMessage.CODE) {
                        InternalMessage m = (InternalMessage) msg;
                        logger.info(m.getSourceDc() + m.getDepClock() + m.getVerb());
                    } else if (msg.getCode() == ClockMessage.CODE) {
                        ClockMessage m = (ClockMessage) msg;
                        logger.info(m.getClock() + "" + m.getVerb());
                    } else if (msg.getCode() == MigrateMessage.CODE) {
                        MigrateMessage m = (MigrateMessage) msg;
                        logger.info(m.getSourceDc() + m.getClock() + m.getVerb());
                    } else {
                        logger.error("Unknown message in final print: " + msg);
                    }
                }
            }

            logger.info("Waiting muts:");
            for(Map.Entry<InetAddress, WaitingState> e : currentMutations.entrySet()){
                logger.info(e.getValue().getMessage() + " " + e.getValue().getNodesMap());
            }
            LogManager.shutdown();
        }));

        while (true) {
            newMessageQueue.poll(1, TimeUnit.SECONDS);

            boolean anyExecuted;
            do {
                anyExecuted = false;
                for (Queue<Message> dcQueue : waitingMessages.values()) {
                    boolean executed;
                    do{
                        executed = false;
                        Message peek = dcQueue.peek();
                        if (peek != null && canExecuteMessage(peek)) {
                            dcQueue.remove();
                            executeMessage(peek);
                            anyExecuted = true;
                            executed = true;
                            MessageReceiverV3.internalMessagesExecuted++;
                        }
                    } while(executed);
                }
            } while (anyExecuted);
            MessageReceiverV3.executorLoops++;
        }
    }

    private void executeMessage(Message msg) {
        if (msg.getCode() == InternalMessage.CODE) {
            InternalMessage m = (InternalMessage) msg;
            logger.debug("Executing internal message " + m);
            executeInternalMessage(m);
        } else if (msg.getCode() == ClockMessage.CODE) {
            ClockMessage m = (ClockMessage) msg;
            logger.debug("Executing clock message " + m);
            executeClockMessage(m);
        } else if (msg.getCode() == MigrateMessage.CODE) {
            MigrateMessage m = (MigrateMessage) msg;
            logger.debug("Executing migrate message " + m);
            executeMigrateMessage(m);
        } else {
            logger.error("Unknown message received in executor: " + msg);
        }
    }

    public static void executeMigrateMessage(MigrateMessage msg) {
        TreeManager tree = TreeManager.getInstance();

        if (msg.getPossibleDatacenters().get(0).equals(tree.getLocalDc())) {
            //target is me, send to client
            logger.debug("Redirecting migrate to client: " + msg.getFrom());
            try {
                ConnectionManager.getConnectionToClient(msg.getFrom()).writeAndFlush(msg);
            } catch (Exception e) {
                logger.error("Failed to migration message to client " + msg.getFrom() + ". " + e.getMessage());
            }
        } else {
            //or not, propagate
            for (String dc : tree.getDownBranches(msg.getSourceDc())) {
                Set<String> targets = tree.getTargetsBehindBranch(msg.getSourceDc(), dc);
                if (targets.contains(msg.getPossibleDatacenters().get(0))) {
                    logger.debug("Redirecting migrate to: " + dc);
                    try {
                        InetAddress dcAddress = InetAddress.getByName(tree.getDcAddress(dc));
                        ConnectionManager.getConnectionToHigh(dcAddress).writeAndFlush(msg);
                    } catch (Exception e) {
                        logger.error("Failed to migration message to " + dc + ". " + e.getMessage());
                    }
                    break;
                }
            }
        }


    }

    private void executeClockMessage(ClockMessage msg) {
        //propagate
        String sourceDc = msg.getClock().left;
        for (String dc : TreeManager.getInstance().getDownBranches(sourceDc)) {
            try {
                InetAddress dcAddress = InetAddress.getByName(TreeManager.getInstance().getDcAddress(dc));
                ConnectionManager.getConnectionToHigh(dcAddress).writeAndFlush(msg);
            } catch (UnknownHostException | InterruptedException e) {
                logger.error("Failed to forward clock to " + dc + ". " + e.getMessage());
            }
        }
        MessageReceiverV3.updateClock(sourceDc, msg.getClock().right);
    }

    public static void executeInternalMessage(InternalMessage msg) {
        String localDc = TreeManager.getInstance().getLocalDc();

        if (msg.getVerb() == Message.VERB_READ_REQUEST || msg.getVerb() == Message.VERB_READ_REPLY) {
            if (msg.getTargets().containsKey(localDc)) {
                sendToLocalNodes(msg);
            }
            propagateInternalMessage(msg);
        } else if (msg.getVerb() == Message.VERB_WRITE) {
            //execute, either send to local nodes, or act as clock
            if (msg.getTargets().containsKey(localDc)) {
                WaitingState put = currentMutations.put(msg.getFrom(),
                        new WaitingState(msg, localDc));
                if (put != null) {
                    logger.error("trying to override current mutation... info:");
                    logger.error("new mutation: " + msg);
                    logger.error("old mutation: " + put.getSourceDc() + " " + put.getClock());
                    logger.error("current clock: " + MessageReceiverV3.clock);
                    System.exit(1);
                }
                sendToLocalNodes(msg);
            } else {
                propagateInternalMessage(msg);
                MessageReceiverV3.updateClock(msg.getSourceDc(), msg.getDepClock().get(msg.getSourceDc()));
            }
        } else {
            assert false;
        }
    }

    public static void propagateInternalMessage(InternalMessage msg) {
        TreeManager tree = TreeManager.getInstance();
        ClockMessage cm = null;

        //propagate
        for (String dc : tree.getDownBranches(msg.getSourceDc())) {

            //Find targets in that branch
            Set<String> targets = tree.getTargetsBehindBranch(msg.getSourceDc(), dc);

            targets.retainAll(msg.getTargets().keySet());

            //Decide message or clock (or none) and create
            Message m = null;
            if (!targets.isEmpty()) {
                m = msg.filterAndClone(targets);
                logger.debug("Redirecting to: " + dc);
            } else if (msg.getVerb() == Message.VERB_WRITE) {
                if (cm == null) cm = ClockMessage.fromInternal(msg);
                logger.debug("Clock to: " + dc + " " + cm);
                m = cm;
            }

            //Send
            if (m != null) {
                try {
                    InetAddress dcAddress = InetAddress.getByName(tree.getDcAddress(dc));
                    ConnectionManager.getConnectionToHigh(dcAddress).writeAndFlush(m);
                } catch (Exception e) {
                    logger.error("Failed to forward message to " + dc + ". " + e.getMessage());
                }
            }
        }
    }

    private static void sendToLocalNodes(InternalMessage msg) {
        logger.debug("Sending to local nodes:" + msg.getDepClock());
        List<Pair<Integer, InetAddress>> localNodes = msg.getTargets().get(TreeManager.getInstance().getLocalDc());
        assert localNodes != null && !localNodes.isEmpty();
        for (Pair<Integer, InetAddress> p : localNodes) {
            DownMessage down = new DownMessage(p.left, msg.getFrom(), msg.getVerb(), msg.getTimestamp());
            try {
                ConnectionManager.getConnectionToLow(p.right).writeAndFlush(down);
            } catch (Exception e) {
                logger.error("Failed to forward message to " + p.right + ". " + e.getMessage());
            }
        }
    }

    public static boolean canExecuteMessage(Message msg) {
        if (msg.getCode() == InternalMessage.CODE) {
            InternalMessage m = (InternalMessage) msg;
            return checkMessageDependencies(m.getDepClock(), m.getSourceDc(), m.getVerb() == Message.VERB_WRITE);
        } else if (msg.getCode() == ClockMessage.CODE) {
            ClockMessage m = (ClockMessage) msg;
            return checkClockDependencies(m.getClock());
        } else if (msg.getCode() == MigrateMessage.CODE) {
            MigrateMessage m = (MigrateMessage) msg;
            return checkMessageDependencies(m.getClock(), m.getSourceDc(), false);
        } else {
            logger.error("Unknown message received 'canExecute': " + msg);
            System.exit(1);
            return false;
        }
    }

    private static boolean checkMessageDependencies(Map<String, Integer> messageClock,
                                                    String sourceDc, boolean isMutation) {
        for (Map.Entry<String, Integer> e : messageClock.entrySet()) {
            String dc = e.getKey();
            int opClock = e.getValue();
            int myClock = MessageReceiverV3.clock.getOrDefault(dc, 0);

            if (dc.equals(sourceDc)) {
                //should always be my clock+1 (never less)
                if (opClock > myClock + (isMutation ? 1 : 0)) {
                    return false;
                }
            } else {
                if (opClock > myClock) {
                    return false;
                }
            }
        }
        return true;
    }

    private static boolean checkClockDependencies(Pair<String, Integer> dep) {
        return dep.right == MessageReceiverV3.clock.getOrDefault(dep.left, 0) + 1;
    }

    public static int getWaitingMessagesSize() {
        return waitingMessages.values().stream().mapToInt(Queue::size).sum();
    }

    public static void addToWaitingMessages(Message msg) {
        MessageReceiverV3.internalMessagesReceived++;
        String sourceDc = null;
        if (msg.getCode() == InternalMessage.CODE) {
            InternalMessage m = (InternalMessage) msg;
            sourceDc = m.getSourceDc();
        } else if (msg.getCode() == ClockMessage.CODE) {
            ClockMessage m = (ClockMessage) msg;
            sourceDc = m.getClock().left;
        } else if (msg.getCode() == MigrateMessage.CODE) {
            MigrateMessage m = (MigrateMessage) msg;
            sourceDc = m.getSourceDc();
        } else {
            logger.error("Unknown message received in addToWaitingMessages: " + msg);
            System.exit(1);
        }
        waitingMessages.computeIfAbsent(sourceDc, k -> new ConcurrentLinkedQueue<>()).add(msg);

        logger.debug("New message from " + sourceDc + ": " + msg);
        signalLoop();
    }

    public static void signalLoop() {
        newMessageQueue.add(dummyObj);
    }

    @Override
    public void run() {
        try {
            mainLoop();
        } catch (Exception e) {
            logger.error("Exception in executor thread: " + e);
            System.exit(1);
        }
    }
}
