package org.pfouto.internal.state;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pfouto.internal.connection.ConnectionManager;
import org.pfouto.internal.messaging.*;
import org.pfouto.internal.tree.TreeManager;
import org.pfouto.internal.utils.Pair;

import java.net.InetAddress;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class MessageHandlerSaturn {

    private static final Logger logger = LogManager.getLogger(MessageHandlerSaturn.class);

    public static BlockingQueue<Message> messageInQueue = new LinkedBlockingQueue<>();
    public static BlockingQueue<Message> ackQueue = new LinkedBlockingQueue<>();

    private WaitingState currentMutation;

    private long lastStatus;
    private static final long STATUS_INTERVAL = 1000;
    private int inSinceLastStatus;
    private int waitingSinceLastStatus;

    private int totalMessages;
    private int totalAck;
    private int totalInternal;
    private int totalClock;
    private int totalUp;


    public MessageHandlerSaturn() {

        currentMutation = null;

        lastStatus = 0;
        inSinceLastStatus = 0;
        waitingSinceLastStatus = 0;
        totalMessages = 0;
        totalUp = 0;
        totalClock = 0;
        totalInternal = 0;
        totalAck = 0;
    }

    public void mainLoop() throws InterruptedException {
        //noinspection InfiniteLoopStatement
        while (true) {

            long now = System.currentTimeMillis();
            if (now - lastStatus > STATUS_INTERVAL) {

                lastStatus = now;

                int inQueueSize = messageInQueue.size();
                String status = "\nQueueSize:\tI " + inQueueSize
                        + "\nExecutedOps:\tI " + inSinceLastStatus + "\tW " + waitingSinceLastStatus
                        + "\nReceivedOps:\tU " + totalUp + "\tI " + totalInternal
                        + "\tC " + totalClock + "\tA " + totalAck + "\tT " + totalMessages;

                if (inQueueSize > 20000) {
                    logger.warn(status);
                } else {
                    logger.info(status);
                }
                inSinceLastStatus = 0;
                waitingSinceLastStatus = 0;
            }

            Message poll;
            if(currentMutation != null){
               poll = ackQueue.take();
            } else {
                poll = messageInQueue.take();
            }
            handleMessage(poll);
            totalMessages++;
            inSinceLastStatus++;
        }
    }

    private void handleMessage(Message msg) {

        switch (msg.getCode()) {
            case UpMessage.CODE: {
                UpMessage m = (UpMessage) msg;
                totalUp++;
                logger.debug("Up message received: " + m);
                handleUpMessage(m);
                break;
            }
            case InternalMessage.CODE: {
                InternalMessage m = (InternalMessage) msg;
                totalInternal++;
                logger.debug("Internal message received: " + m);
                handleInternalMessage(m);
                break;
            }
            case ClockMessage.CODE: {
                logger.error("Clock message received. this is saturn...");
                System.exit(1);
                break;
            }
            case AckMessage.CODE: {
                totalAck++;
                AckMessage m = (AckMessage) msg;
                logger.debug("Ack message received " + m);
                handleAckMessage(m);
                break;
            }
            default: {
                logger.error("Unknown message received: " + msg);
            }
        }
    }

    private void handleUpMessage(UpMessage msg) {
        String localDc = TreeManager.getInstance().getLocalDc();

        InternalMessage internalMessage =
                new InternalMessage(msg.getTargets(), localDc, 0, Collections.emptyMap(), msg.getFrom(), msg.getVerb(), msg.getTimestamp());
        handleInternalMessage(internalMessage);
    }

    private void handleInternalMessage(InternalMessage msg) {
        TreeManager tree = TreeManager.getInstance();

        //propagate
        for (String dc : tree.getDownBranches(tree.getMainTree())) {

            //Find targets in that branch
            Set<String> targets = tree.getTargetsBehindBranch(tree.getMainTree(), dc);

            targets.retainAll(msg.getTargets().keySet());

            //Decide message or clock (or none) and create
            Message m = null;
            if (!targets.isEmpty()) {
                m = msg.filterAndClone(targets);
                logger.debug("Redirecting to: " + dc);
            }
            //Send
            if (m != null) {
                try {
                    InetAddress dcAddress = InetAddress.getByName(tree.getDcAddress(dc));
                    ConnectionManager.getConnectionToHigh(dcAddress).writeAndFlush(m);
                } catch (Exception e) {
                    logger.error("Failed to forward message to " + dc + ". " + e.getMessage());
                }
            }
        }
        //if not for me, nothing to do here
        if (!msg.getTargets().containsKey(tree.getLocalDc()))
            return;

        //if for me, execute
        executeInternalMessage(msg);
    }

    private void executeInternalMessage(InternalMessage msg) {
        String localDc = TreeManager.getInstance().getLocalDc();

        assert currentMutation == null;

        if (msg.getVerb() == Message.VERB_READ_REQUEST || msg.getVerb() == Message.VERB_READ_REPLY) {
            assert msg.getTargets().containsKey(localDc);
            sendToLocalNodes(msg);
        } else if (msg.getVerb() == Message.VERB_WRITE) {
            currentMutation = new WaitingState(msg, localDc);
            sendToLocalNodes(msg);
        } else {
            assert false;
        }
    }

    private void handleAckMessage(AckMessage msg) {
        if (currentMutation != null) {
            currentMutation.receiveAck(msg.getNode(), msg.getId());
            if (currentMutation.isCompleted()) {
                logger.debug("Mutation completed");
                currentMutation = null;
            }
        } else {
            //TODO remove this error
            logger.error("Unneeded ack received (because quorum is 1)");
        }
    }

    private void sendToLocalNodes(InternalMessage msg) {
        logger.debug("Sending to local nodes:" + msg.getDepClock());
        //send to nodes
        List<Pair<Integer, InetAddress>> localNodes = msg.getTargets().get(TreeManager.getInstance().getLocalDc());

        assert localNodes != null && !localNodes.isEmpty();
        for (Pair<Integer, InetAddress> p : localNodes) {
            DownMessage down = new DownMessage(p.left, msg.getFrom(), msg.getVerb(), msg.getTimestamp());
            try {
                ConnectionManager.getConnectionToLow(p.right).writeAndFlush(down);
            } catch (Exception e) {
                logger.error("Failed to forward message to " + p.right + ". " + e.getMessage());
            }
        }
    }
}
