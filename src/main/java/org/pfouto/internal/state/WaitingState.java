package org.pfouto.internal.state;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pfouto.internal.messaging.InternalMessage;
import org.pfouto.internal.utils.Pair;

import java.net.InetAddress;
import java.util.*;

public class WaitingState {

    private static final Logger logger = LogManager.getLogger(WaitingState.class);

    private Map<Pair<Integer, InetAddress>, Boolean> nodesMap;
    private InternalMessage message;
    private boolean completed;

    public WaitingState(InternalMessage message, String localDc) {
        this.nodesMap = new HashMap<>();
        for (Pair<Integer, InetAddress> p : message.getTargets().get(localDc)) {
            nodesMap.put(p, false);
        }
        this.message = message;
        completed = false;
    }

    public boolean receiveAck(InetAddress node, int messageId) {
        if (!completed) {
            Pair<Integer, InetAddress> pair = Pair.create(messageId, node);
            if (nodesMap.containsKey(pair)) {
                nodesMap.put(pair, true);
                return true;
            }
        }
        return false;
    }

    //Only return true ONCE - signaling mutation completion
    public boolean isCompleted() {
        boolean ret = (!completed && hasEnoughAcks());
        if (ret)
            completed = true;
        return ret;
    }


    private boolean hasEnoughAcks() {
        int nAcks = 0;
        for (Boolean b : nodesMap.values())
            if (b) nAcks++;
        return nAcks >= ((nodesMap.size() / 2) + 1);
    }

    public Map<Pair<Integer, InetAddress>, Boolean> getNodesMap() {
        return nodesMap;
    }

    public Map<String, Integer> getClock() {
        return message.getDepClock();
    }

    public String getSourceDc() {
        return message.getSourceDc();
    }

    public InternalMessage getMessage() {
        return message;
    }

    public List<Pair<InetAddress, Integer>> getMapKeys() {
        List<Pair<InetAddress, Integer>> ret = new LinkedList<>();
        nodesMap.keySet().forEach(k -> ret.add(Pair.create(message.getFrom(), k.left)));
        return ret;
    }

}
