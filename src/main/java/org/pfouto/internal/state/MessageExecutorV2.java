package org.pfouto.internal.state;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pfouto.internal.connection.ConnectionManager;
import org.pfouto.internal.messaging.*;
import org.pfouto.internal.tree.TreeManager;
import org.pfouto.internal.utils.Pair;

import java.net.InetAddress;
import java.util.*;
import java.util.concurrent.*;

public class MessageExecutorV2 implements Runnable {

    private static final Logger logger = LogManager.getLogger(MessageExecutorV2.class);

    //                  sourceDc      message
    protected static Map<String, Queue<Message>> waitingMessages;

    private static Object dummyObj = new Object();
    private static BlockingQueue<Object> newMessageQueue;

    //                      from
    protected static Map<InetAddress, WaitingState> currentMutations;

    public MessageExecutorV2() {
        waitingMessages = new ConcurrentHashMap<>();
        currentMutations = new ConcurrentHashMap<>();
        newMessageQueue = new LinkedBlockingQueue<>();
    }

    private void mainLoop() throws InterruptedException {


        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            for (Map.Entry<String, Queue<Message>> e : waitingMessages.entrySet()) {
                String k = e.getKey();
                Queue<Message> v = e.getValue();
                logger.info(k + ":" + v.size());
                int messages = 10;
                while (!v.isEmpty() && messages > 0) {
                    messages--;
                    Message msg = v.poll();
                    if (msg.getCode() == InternalMessage.CODE) {
                        InternalMessage m = (InternalMessage) msg;
                        logger.info(m.getSourceDc() + m.getDepClock() + m.getVerb());
                    } else if (msg.getCode() == ClockMessage.CODE) {
                        ClockMessage m = (ClockMessage) msg;
                        logger.info(m.getClock() + "" + m.getVerb());
                    } else if (msg.getCode() == MigrateMessage.CODE) {
                        MigrateMessage m = (MigrateMessage) msg;
                        logger.info(m.getSourceDc() + m.getClock() + m.getVerb());
                    } else {
                        logger.error("Unknown message in final print: " + msg);
                    }
                }
            }

            logger.info("Waiting muts:");
            for(Map.Entry<InetAddress, WaitingState> e : currentMutations.entrySet()){
                logger.info(e.getValue().getMessage() + " " + e.getValue().getNodesMap());
            }
            LogManager.shutdown();
        }));


        //noinspection InfiniteLoopStatement
        while (true) {
            newMessageQueue.take();
            boolean anyExecuted;
            do {
                anyExecuted = false;

                for (Queue<Message> dcQueue : waitingMessages.values()) {
                    boolean executed;
                    do {
                        executed = false;
                        Message peek = dcQueue.peek();
                        if (peek != null && canExecuteMessage(peek)) {
                            dcQueue.remove();
                            executeMessage(peek);
                            MessageReceiverV2.messagesExecutedByExecutor++;
                            executed = true;
                            anyExecuted = true;
                        }
                    }
                    while (executed);
                }
            } while (anyExecuted);
            MessageReceiverV2.executorLoops++;
        }
    }

    private void executeMessage(Message msg) {
        if (msg.getCode() == InternalMessage.CODE) {
            InternalMessage m = (InternalMessage) msg;
            executeInternalMessage(m);
        } else if (msg.getCode() == ClockMessage.CODE) {
            ClockMessage m = (ClockMessage) msg;
            executeClockMessage(m);
        } else if (msg.getCode() == MigrateMessage.CODE) {
            MigrateMessage m = (MigrateMessage) msg;
            executeMigrateMessage(m);
        } else {
            logger.error("Unknown message received in executor: " + msg);
        }
    }

    private void executeMigrateMessage(MigrateMessage msg) {
        TreeManager tree = TreeManager.getInstance();
        assert msg.getPossibleDatacenters().get(0).equals(tree.getLocalDc());
        logger.debug("Redirecting migrate to client: " + msg.getFrom());
        try {
            ConnectionManager.getConnectionToClient(msg.getFrom()).writeAndFlush(msg);
        } catch (Exception e) {
            logger.error("Failed to migration message to client " + msg.getFrom() + ". " + e.getMessage());
        }
    }

    private void executeClockMessage(ClockMessage msg) {
        updateClock(msg.getClock().left, msg.getClock().right);
    }

    private void executeInternalMessage(InternalMessage msg) {
        String localDc = TreeManager.getInstance().getLocalDc();

        if (msg.getVerb() == Message.VERB_READ_REQUEST || msg.getVerb() == Message.VERB_READ_REPLY) {
            sendToLocalNodes(msg);
        } else if (msg.getVerb() == Message.VERB_WRITE) {
            //execute, either send to local nodes, or act as clock
            if (msg.getTargets().containsKey(localDc)) {
                WaitingState put = currentMutations.put(msg.getFrom(),
                        new WaitingState(msg, localDc));
                if (put != null) {
                    logger.error("trying to override current mutation... info:");
                    logger.error("new mutation: " + msg);
                    logger.error("old mutation: " + put.getSourceDc() + " " + put.getClock());
                    logger.error("current clock: " + MessageReceiverV2.clock);
                    System.exit(0);
                }
                sendToLocalNodes(msg);
            } else {
                updateClock(msg.getSourceDc(), msg.getDepClock().get(msg.getSourceDc()));
            }
        } else {
            assert false;
        }
    }

    private void sendToLocalNodes(InternalMessage msg) {
        logger.debug("Sending to local nodes:" + msg.getDepClock());
        List<Pair<Integer, InetAddress>> localNodes = msg.getTargets().get(TreeManager.getInstance().getLocalDc());
        assert localNodes != null && !localNodes.isEmpty();
        for (Pair<Integer, InetAddress> p : localNodes) {
            DownMessage down = new DownMessage(p.left, msg.getFrom(), msg.getVerb(), msg.getTimestamp());
            try {
                ConnectionManager.getConnectionToLow(p.right).writeAndFlush(down);
            } catch (Exception e) {
                logger.error("Failed to forward message to " + p.right + ". " + e.getMessage());
            }
        }
    }

    //also removes clock positions that pass the check
    private boolean canExecuteMessage(Message msg) {
        if (msg.getCode() == InternalMessage.CODE) {
            InternalMessage m = (InternalMessage) msg;
            return checkMessageDependencies(m.getDepClock(), m.getSourceDc(), m.getVerb() == Message.VERB_WRITE);
        } else if (msg.getCode() == ClockMessage.CODE) {
            return checkClockDependencies(((ClockMessage) msg).getClock());
        } else if (msg.getCode() == MigrateMessage.CODE) {
            MigrateMessage m = (MigrateMessage) msg;
            return checkMessageDependencies(m.getClock(), m.getSourceDc(), false);
        } else {
            logger.error("Unknown message received 'canExecute': " + msg);
            System.exit(1);
            return false;
        }
    }

    //Returns null if can execute, or a missing clock if not
    private boolean checkMessageDependencies(Map<String, Integer> messageClock,
                                             String sourceDc, boolean isMutation) {
        Iterator<Map.Entry<String, Integer>> it = messageClock.entrySet().iterator();
        boolean canExec = true;
        while (it.hasNext()) {
            Map.Entry<String, Integer> e = it.next();
            String dc = e.getKey();
            int opClock = e.getValue();
            int myClock = MessageReceiverV2.clock.getOrDefault(dc, 0);

            if (dc.equals(sourceDc)) {
                //should always be my clock+1 (never less)
                if (opClock > myClock + (isMutation ? 1 : 0)) {
                    canExec = false;
                }
                //no else, since we dont want to remove this clock pos for writes
            } else {
                if (opClock > myClock) {
                    canExec = false;
                } else {
                    it.remove();
                }
            }
        }
        return canExec;
    }

    private boolean checkClockDependencies(Pair<String, Integer> dep) {
        return dep.right == MessageReceiverV2.clock.getOrDefault(dep.left, 0) + 1;
    }

    private void updateClock(String dc, int val) {
        int current = MessageReceiverV2.clock.getOrDefault(dc, 0);
        assert val == current + 1;
        MessageReceiverV2.clock.put(dc, val);
        logger.debug("New clock: " + MessageReceiverV2.clock);
    }

    public static int getWaitingMessagesSize() {
        return waitingMessages.values().stream().mapToInt(Queue::size).sum();
    }
    public static int getWaitingMessagesLocalSize() {
        Queue<Message> messages = waitingMessages.get(TreeManager.getInstance().getLocalDc());
        return messages == null ? -1 : messages.size();
    }

    public static void addToWaitingMessages(String dc, Message m) {
        MessageReceiverV2.messagesStored++;
        waitingMessages.computeIfAbsent(dc, k -> new ConcurrentLinkedQueue<>()).add(m);
        signalLoop();
    }

    public static void signalLoop() {
        newMessageQueue.offer(dummyObj);
    }

    @Override
    public void run() {
        try {
            mainLoop();
        } catch (Exception e) {
            logger.error("Exception in executor thread: " + e);
            System.exit(1);
        }
    }
}
