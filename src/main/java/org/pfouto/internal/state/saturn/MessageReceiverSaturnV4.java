package org.pfouto.internal.state.saturn;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pfouto.internal.connection.ConnectionManager;
import org.pfouto.internal.messaging.*;
import org.pfouto.internal.state.WaitingState;
import org.pfouto.internal.state.v4.MessageExecutorV4;
import org.pfouto.internal.tree.TreeManager;
import org.pfouto.internal.utils.Pair;

import java.net.InetAddress;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;

public class MessageReceiverSaturnV4 implements Runnable {

    private static final Logger logger = LogManager.getLogger(MessageReceiverSaturnV4.class);

    private static BlockingQueue<Message> localQueue = new LinkedBlockingQueue<>();

    private static Queue<Pair<Integer, Message>> labelSink = new LinkedBlockingQueue<>();

    private long lastStatus;
    private static final long STATUS_INTERVAL = 1000;

    private Map<String, Pair<Integer, Long>> internalsStatus = new HashMap<>();

    //logs
    static int executorLoops = 0;
    int clientMessagesExecuted = 0;
    static int internalMessagesExecuted = 0;
    int mutationsCompleted = 0;
    int totalMutations = 0;
    static int totalIntExec = 0;

    static int clientMessagesReceived = 0;
    static int internalMessagesReceived = 0;

    static int maxCurrentMutations = 0;

    public MessageReceiverSaturnV4() {
        lastStatus = System.currentTimeMillis();
    }

    private void mainLoop() throws InterruptedException {
        //noinspection InfiniteLoopStatement
        while (true) {

            Message in = localQueue.poll(500, TimeUnit.MILLISECONDS);
            if (in != null) {
                handleNoDepMessage(in);
                clientMessagesExecuted++;
            }

            long now = System.currentTimeMillis();
            if (now - lastStatus > STATUS_INTERVAL) {
                int inQueueSize = localQueue.size();
                int labelSinkSize = labelSink.size();
                int waitingSize = MessageExecutorSaturnV4.waitingMessagesRemote.size();
                //propagate info
                propagateSaturationState(inQueueSize + labelSinkSize + waitingSize);
                outputLogs(inQueueSize, labelSinkSize, waitingSize);
                lastStatus = now;
            }
        }
    }

    private void outputLogs(int inQueueSize, int labelSinkSize, int waitingSize) {
        totalIntExec += internalMessagesExecuted;

        //LOG stuff
        StringBuilder sb = new StringBuilder();

        sb.append("\nInQ: ").append(inQueueSize).append("\n");
        sb.append("LabelSink: ").append(labelSinkSize).append("\n");
        sb.append("WaitingRemote: ").append(waitingSize).append("\n");

        sb      .append("InRec: ").append(clientMessagesReceived)
                .append("\tInExec: ").append(clientMessagesExecuted)
                .append("\nIntRec: ").append(internalMessagesReceived)
                .append("\tIntExec: ").append(internalMessagesExecuted)
                .append("(").append(totalIntExec).append(")")
                .append("\tLoops: ").append(executorLoops)
                .append("\tMutComplet: ").append(mutationsCompleted)
                .append("(").append(totalMutations).append(")");
        sb.append("\n");

        sb.append("CurrentMut: " + MessageExecutorSaturnV4.executingLabels.size())
                .append("(").append(maxCurrentMutations).append(")")
                .append("\n");

        if (inQueueSize > 100000 || waitingSize > 100000 || labelSinkSize > 100000) {
            logger.error(sb.toString());
        } else if (inQueueSize > 20000 || waitingSize > 20000 || labelSinkSize > 20000) {
            logger.warn(sb.toString());
        } else {
            logger.info(sb.toString());
        }

        internalMessagesReceived = 0;
        clientMessagesReceived = 0;

        executorLoops = 0;
        clientMessagesExecuted = 0;
        internalMessagesExecuted = 0;
        mutationsCompleted = 0;
    }

    private void handleNoDepMessage(Message msg) {
        if (msg.getCode() == UpMessage.CODE) {
            UpMessage m = (UpMessage) msg;
            logger.debug("Up message received: " + m);
            handleUpMessage(m);
        } else if (msg.getCode() == AckMessage.CODE) {
            AckMessage m = (AckMessage) msg;
            logger.debug("Ack message received " + m);
            handleAckMessage(m);
        } else if (msg.getCode() == StatusMessage.CODE) {
            StatusMessage m = (StatusMessage) msg;
            //logger.debug("Status message received");
            handleStatusMessage(m);
        } else if (msg.getCode() == MigrateMessage.CODE) {
            MigrateMessage m = (MigrateMessage) msg;
            logger.debug("Migrate message received " + m);
            handleMigrateMessage(m);
        } else {
            logger.error("Unknown message received in receiver: " + msg);
        }
    }

    private void handleUpMessage(UpMessage msg) {
        assert msg.getSaturnLabel() != -1;
        String localDc = TreeManager.getInstance().getLocalDc();
        InternalMessage internalMessage =
                new InternalMessage(msg.getTargets(), localDc, 0, Collections.emptyMap(),
                        msg.getSaturnLabel(), msg.getFrom(), msg.getVerb(), msg.getTimestamp());
        addToLabelSink(internalMessage);
    }

    private void addToLabelSink(Message msg) {
        if (msg instanceof InternalMessage) {
            labelSink.add(Pair.create(((InternalMessage) msg).getSaturnLabel(), msg));
        } else {
            labelSink.add(Pair.create(((MigrateMessage) msg).getLabelSaturn(), msg));
        }
    }

    private void handleMigrateMessage(MigrateMessage m) {

        TreeManager tree = TreeManager.getInstance();

        Map<String, Integer> opClock = m.getClock();
        List<String> possibleDcs = m.getPossibleDatacenters();

        assert m.getSourceDc().equals(tree.getLocalDc());
        assert opClock == null || opClock.isEmpty();

        String bestDc = null;

        /*
        int myIndex = -1;
        for (int i = 0; i < Data.NODES.length; i++) {
            if (Data.NODES[i].equals(TreeManager.getInstance().getLocalDc())) {
                myIndex = i;
            }
        }
        assert myIndex != -1;


        int bestDcDist = Integer.MAX_VALUE;
        for (int i = 0; i < Data.NODES.length; i++) {
            if (possibleDcs.contains(Data.NODES[i])) {
                int dist = Data.LATENCIES[myIndex][i];
                if (dist < bestDcDist) {
                    bestDcDist = dist;
                    bestDc = Data.NODES[i];
                }
            }
        }*/

        int bestDcLoad = Integer.MAX_VALUE;
        for (String dc : possibleDcs) {
            Pair<Integer, Long> pair = internalsStatus.get(dc);
            if (pair == null)
                continue;
            int load = pair.left;
            if (load < bestDcLoad) {
                bestDcLoad = load;
                bestDc = dc;
            }
        }
        assert bestDc != null;

        possibleDcs = new LinkedList<>();
        possibleDcs.add(bestDc);
        opClock = Collections.emptyMap();

        MigrateMessage newMsg = new MigrateMessage(m.getThread(), m.getSourceDc(), possibleDcs,
                m.getLabelSaturn(), m.getSrcSaturn(), opClock, m.getFrom(), m.getVerb(), m.getTimestamp());
        assert newMsg.getPossibleDatacenters().size() == 1;

        addToLabelSink(newMsg);
    }

    private void handleStatusMessage(StatusMessage sm) {
        for (Map.Entry<String, Pair<Integer, Long>> entry : sm.getStatus().entrySet()) {
            String dc = entry.getKey();
            Pair<Integer, Long> remotePair = entry.getValue();
            Pair<Integer, Long> localPair = internalsStatus.get(dc);
            if (localPair == null || remotePair.right > localPair.right) {
                internalsStatus.put(dc, remotePair);
            }
        }
    }

    private void handleAckMessage(AckMessage msg) {
        Pair p = Pair.create(msg.getFrom(), msg.getId());
        WaitingState waitingState = MessageExecutorSaturnV4.currentMutations.remove(p);
        if (waitingState != null) {
            waitingState.receiveAck(msg.getNode(), msg.getId());
            if (waitingState.isCompleted()) {
                String sourceDc = waitingState.getSourceDc();
                logger.debug("Mutation completed " + sourceDc + " " + waitingState.getMessage().getSaturnLabel());
                MessageExecutorSaturnV4.executingLabels.remove(
                        Pair.create(waitingState.getMessage().getSaturnLabel(), msg.getFrom())
                );
                MessageExecutorSaturnV4.signalLoop();
                mutationsCompleted++;
                totalMutations++;
            }
        }
    }

    private void propagateSaturationState(int totalQueueSizes) {
        TreeManager tree = TreeManager.getInstance();

        long time = System.currentTimeMillis();
        internalsStatus.put(tree.getLocalDc(), Pair.create(totalQueueSizes, time));
        StatusMessage sm = new StatusMessage(time, tree.getLocalDc(), new HashMap<>(internalsStatus));

        List<String> targets = new LinkedList<>();
        targets.addAll(tree.getDownBranches(tree.getMainTree()));
        String upBranch = tree.getUpBranch(tree.getMainTree());
        if (upBranch != null) targets.add(upBranch);

        for (String dc : targets) {
            try {
                InetAddress dcAddress = InetAddress.getByName(tree.getDcAddress(dc));
                ConnectionManager.getConnectionToHigh(dcAddress).writeAndFlush(sm);
            } catch (Exception e) {
                logger.error("Failed to send status message to " + dc + ". " + e.getMessage());
            }
        }
    }

    public static void addToLocalQueue(Message msg) {
        clientMessagesReceived++;
        localQueue.add(msg);
    }

    @Override
    public void run() {
        try {
            Timer timer = new Timer();
            TimerTask propagateSink = new TimerTask() {
                @Override
                public void run() {
                    int nMessages = labelSink.size();
                    if(nMessages == 0){
                        return;
                    }
                    Queue<Pair<Integer, Message>> sendQueue =
                            new PriorityBlockingQueue<>(nMessages, Comparator.comparing(p -> p.left));
                    for (int i = 0; i < nMessages; i++) {
                        sendQueue.add(labelSink.remove());
                    }
                    while (!sendQueue.isEmpty()) {
                        Message msg = sendQueue.poll().right;
                        if (msg != null) {
                            if (msg instanceof InternalMessage) {
                                MessageExecutorSaturnV4.
                                        propagateInternalMessage((InternalMessage) msg);
                            } else {
                                MessageExecutorSaturnV4.
                                        propagateMigrateMessage((MigrateMessage) msg);
                            }
                        }
                    }
                }
            };
            timer.schedule(propagateSink, 100, 100);

            mainLoop();

        } catch (Exception e) {
            logger.error("Exception in receiver thread: " + e);
            e.printStackTrace();
            System.exit(1);
        }
    }

}
