package org.pfouto.internal.state.saturn;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pfouto.internal.connection.ConnectionManager;
import org.pfouto.internal.main.Main;
import org.pfouto.internal.messaging.*;
import org.pfouto.internal.state.WaitingState;
import org.pfouto.internal.tree.TreeManager;
import org.pfouto.internal.utils.Pair;

import java.net.InetAddress;
import java.util.*;
import java.util.concurrent.*;

public class MessageExecutorSaturnV4 implements Runnable {

    private static final Logger logger = LogManager.getLogger(MessageExecutorSaturnV4.class);

    //                  sourceDc      message
    protected static Queue<Message> waitingMessagesRemote;

    private static Object dummyObj = new Object();
    private static BlockingQueue<Object> newMessageQueue;

    //                          from        mId
    protected static Map<Pair<InetAddress, Integer>, WaitingState> currentMutations;
    protected static ConcurrentHashMap.KeySetView<Pair<Integer, InetAddress>, Boolean> executingLabels;

    public MessageExecutorSaturnV4() {
        executingLabels = ConcurrentHashMap.newKeySet();
        waitingMessagesRemote = new LinkedBlockingQueue<>();
        currentMutations = new ConcurrentHashMap<>();
        newMessageQueue = new LinkedBlockingQueue<>();
    }

    private void mainLoop() throws InterruptedException {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            logger.info("Waiting remote: " + waitingMessagesRemote.size());
            int messages = 10;
            while (!waitingMessagesRemote.isEmpty() && messages > 0) {
                messages--;
                Message msg = waitingMessagesRemote.poll();
                if (msg.getCode() == InternalMessage.CODE) {
                    InternalMessage m = (InternalMessage) msg;
                    logger.info(m.getSourceDc() + m.getDepClock() + m.getVerb());
                } else if (msg.getCode() == ClockMessage.CODE) {
                    ClockMessage m = (ClockMessage) msg;
                    logger.info(m.getClock() + "" + m.getVerb());
                } else if (msg.getCode() == MigrateMessage.CODE) {
                    MigrateMessage m = (MigrateMessage) msg;
                    logger.info(m.getSourceDc() + m.getClock() + m.getVerb());
                } else {
                    logger.error("Unknown message in final print: " + msg);
                }
            }

            logger.info("Waiting muts:");
            for (Map.Entry<Pair<InetAddress, Integer>, WaitingState> e : currentMutations.entrySet()) {
                logger.info(e.getKey() + " " + e.getValue().getMessage() + " " + e.getValue().getNodesMap());
            }
            LogManager.shutdown();
        }));

        //noinspection InfiniteLoopStatement
        while (true) {
            newMessageQueue.poll(100, TimeUnit.MILLISECONDS);
            boolean executed = true;
            while (executed) {
                executed = false;
                Message peek = waitingMessagesRemote.peek();
                if (peek != null && canExecuteMessage(peek)) {
                    waitingMessagesRemote.remove();
                    executeMessage(peek);
                    executed = true;
                    MessageReceiverSaturnV4.internalMessagesExecuted++;
                }
            }
            MessageReceiverSaturnV4.executorLoops++;
        }
    }

    private boolean canExecuteMessage(Message peek) {
        InetAddress lblFrom = peek instanceof MigrateMessage ?
                ((MigrateMessage) peek).getSrcSaturn() :
                peek.getFrom();
        int lblTs = peek instanceof MigrateMessage ?
                ((MigrateMessage) peek).getLabelSaturn() :
                ((InternalMessage) peek).getSaturnLabel();

        for (Pair<Integer, InetAddress> p : executingLabels) {
            if (p.left < lblTs ||
                    (p.left == lblTs && p.right.getHostAddress().compareTo(lblFrom.getHostAddress()) < 0))
                return false;
        }
        return true;
    }

    private void executeMessage(Message msg) {
        if (msg.getCode() == InternalMessage.CODE) {
            InternalMessage m = (InternalMessage) msg;
            logger.debug("Executing internal message " + m);
            executeInternalMessage(m);
        } else if (msg.getCode() == MigrateMessage.CODE) {
            MigrateMessage m = (MigrateMessage) msg;
            logger.debug("Executing migrate message " + m);
            executeMigrateMessage(m);
        } else {
            logger.error("Unknown message received in executor: " + msg);
        }
    }

    private static void executeMigrateMessage(MigrateMessage msg) {
        TreeManager tree = TreeManager.getInstance();

        assert msg.getPossibleDatacenters().get(0).equals(tree.getLocalDc());

        //target is me, send to client
        logger.debug("Redirecting migrate to client: " + msg.getFrom());
        try {
            ConnectionManager.getConnectionToClient(msg.getFrom()).writeAndFlush(msg);
        } catch (Exception e) {
            logger.error("Failed to migration message to client " + msg.getFrom() + ". " + e.getMessage());
        }
    }

    private static void executeInternalMessage(InternalMessage msg) {
        String localDc = TreeManager.getInstance().getLocalDc();

        assert Message.VERB_WRITE == msg.getVerb();
        assert msg.getTargets().containsKey(localDc);

        executingLabels.add(Pair.create(msg.getSaturnLabel(), msg.getFrom()));
        int execSize = executingLabels.size();
        if(execSize > MessageReceiverSaturnV4.maxCurrentMutations){
            MessageReceiverSaturnV4.maxCurrentMutations = execSize;
        }

        WaitingState waitingState = new WaitingState(msg, localDc);
        for (Pair<InetAddress, Integer> key : waitingState.getMapKeys()) {
            WaitingState put = currentMutations.put(key, waitingState);
            assert put == null;
        }
        sendToLocalNodes(msg);
    }

    static void propagateInternalMessage(InternalMessage msg) {
        TreeManager tree = TreeManager.getInstance();

        //propagate
        Set<String> remainingTargets = new HashSet<>(msg.getTargets().keySet());
        remainingTargets.remove(tree.getLocalDc());
        for (String dc : tree.getDownBranches(tree.getMainTree())) {
            //Find targets in that branch
            Set<String> targets = tree.getTargetsBehindBranch(tree.getMainTree(), dc);

            targets.retainAll(msg.getTargets().keySet());

            if (!targets.isEmpty()) {
                Message m = msg.filterAndClone(targets);
                logger.debug("Redirecting to: " + dc);
                try {
                    InetAddress dcAddress = InetAddress.getByName(tree.getDcAddress(dc));
                    ConnectionManager.getConnectionToHigh(dcAddress).writeAndFlush(m);
                } catch (Exception e) {
                    logger.error("Failed to forward message to " + dc + ". " + e.getMessage());
                }
            }
            remainingTargets.removeAll(targets);
        }
        if (!remainingTargets.isEmpty()) {
            Message m = msg.filterAndClone(remainingTargets);
            String dc = tree.getUpBranch(tree.getMainTree());
            assert dc != null;
            logger.debug("Redirecting to: " + dc);
            try {
                InetAddress dcAddress = InetAddress.getByName(tree.getDcAddress(dc));
                ConnectionManager.getConnectionToHigh(dcAddress).writeAndFlush(m);
            } catch (Exception e) {
                logger.error("Failed to forward message to " + dc + ". " + e.getMessage());
            }
        }
    }

    private static void sendToLocalNodes(InternalMessage msg) {
        logger.debug("Sending to local nodes:" + msg.getDepClock());
        List<Pair<Integer, InetAddress>> localNodes = msg.getTargets().get(TreeManager.getInstance().getLocalDc());
        assert localNodes != null && !localNodes.isEmpty();
        for (Pair<Integer, InetAddress> p : localNodes) {
            DownMessage down = new DownMessage(p.left, msg.getFrom(), msg.getVerb(), msg.getTimestamp());
            try {
                ConnectionManager.getConnectionToLow(p.right).writeAndFlush(down);
            } catch (Exception e) {
                logger.error("Failed to forward message to " + p.right + ". " + e.getMessage());
            }
        }
    }

    public static void propagateMigrateMessage(MigrateMessage m) {
        TreeManager tree = TreeManager.getInstance();
        boolean foundDown = false;
        for (String dc : tree.getDownBranches(tree.getMainTree())) {
            Set<String> targets = tree.getTargetsBehindBranch(tree.getMainTree(), dc);
            if (targets.contains(m.getPossibleDatacenters().get(0))) {
                foundDown = true;
                logger.debug("Redirecting migrate to: " + dc);
                try {
                    InetAddress dcAddress = InetAddress.getByName(tree.getDcAddress(dc));
                    ConnectionManager.getConnectionToHigh(dcAddress).writeAndFlush(m);
                } catch (Exception e) {
                    logger.error("Failed to migration message to " + dc + ". " + e.getMessage());
                }
                break;
            }
        }
        if (!foundDown) {
            String dc = tree.getUpBranch(tree.getMainTree());
            assert dc != null;
            logger.debug("Redirecting migrate to: " + dc);
            try {
                InetAddress dcAddress = InetAddress.getByName(tree.getDcAddress(dc));
                ConnectionManager.getConnectionToHigh(dcAddress).writeAndFlush(m);
            } catch (Exception e) {
                logger.error("Failed to migration message to " + dc + ". " + e.getMessage());
            }
        }
    }

    public static void propagateAndMaybeKeep(Message msg) {
        logger.debug("New message: " + msg);
        MessageReceiverSaturnV4.internalMessagesReceived++;
        TreeManager tree = TreeManager.getInstance();
        if (msg.getCode() == InternalMessage.CODE) {
            InternalMessage m = (InternalMessage) msg;
            if (m.getTargets().containsKey(tree.getLocalDc())) {
                waitingMessagesRemote.add(m);
                MessageReceiverSaturnV4.internalMessagesReceived++;
            }
            propagateInternalMessage(m);
        } else if (msg.getCode() == MigrateMessage.CODE) {
            MigrateMessage m = (MigrateMessage) msg;
            if (m.getPossibleDatacenters().get(0).equals(tree.getLocalDc())) {
                waitingMessagesRemote.add(m);
                MessageReceiverSaturnV4.internalMessagesReceived++;
                signalLoop();
            } else {
                propagateMigrateMessage(m);
            }
        } else {
            logger.error("Unknown message received in executor propagate and maybe keep: " + msg);
        }
    }

    public static void signalLoop() {
        newMessageQueue.add(dummyObj);
    }

    @Override
    public void run() {
        try {
            mainLoop();
        } catch (Exception e) {
            logger.error("Exception in executor thread: " + e);
            System.exit(1);
        }
    }
}
