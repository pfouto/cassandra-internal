package org.pfouto.internal.state;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pfouto.internal.connection.ConnectionManager;
import org.pfouto.internal.messaging.*;
import org.pfouto.internal.tree.TreeManager;
import org.pfouto.internal.utils.Pair;

import java.net.InetAddress;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class MessageReceiverV3 implements Runnable {

    private static final Logger logger = LogManager.getLogger(MessageReceiverV3.class);

    private static BlockingQueue<Message> noDependencyQueue = new LinkedBlockingQueue<>();
    private static Queue<InternalMessage> waitingLocalMutations = new LinkedList<>();

    private int mutationClock;
    protected static final Map<String, Integer> clock = new ConcurrentHashMap<>();

    private long lastStatus;
    private static final long STATUS_INTERVAL = 3000;

    private Map<String, Pair<Integer, Long>> internalsStatus = new HashMap<>();

    //logs
    static int executorLoops = 0;
    int clientMessagesExecuted = 0;
    int localMutationsExecuted = 0;
    static int internalMessagesExecuted = 0;
    int mutationsCompleted = 0;
    int clientMessagesStored = 0;
    int totalMutations = 0;
    static int totalIntExec = 0;

    static int clientMessagesReceived = 0;
    static int internalMessagesReceived = 0;

    private static boolean localClockIncremented;

    public MessageReceiverV3() {
        mutationClock = 0;
        lastStatus = System.currentTimeMillis();

        localClockIncremented = true;
    }

    private void mainLoop() throws InterruptedException {
        //noinspection InfiniteLoopStatement
        while (true) {

            Message in = noDependencyQueue.poll(1000, TimeUnit.MILLISECONDS);
            if (in != null) {
                handleNoDepMessage(in);
                clientMessagesExecuted++;
            }

            if (localClockIncremented) {
                InternalMessage localMutation = waitingLocalMutations.poll();
                if (localMutation != null) {
                    localClockIncremented = false;
                    assert MessageExecutorV3.canExecuteMessage(localMutation);
                    MessageExecutorV3.executeInternalMessage(localMutation);
                    localMutationsExecuted++;
                }
            }

            long now = System.currentTimeMillis();
            if (now - lastStatus > STATUS_INTERVAL) {
                lastStatus = now;
                int inQueueSize = noDependencyQueue.size();
                int waitingSize = MessageExecutorV3.getWaitingMessagesSize();
                int localMutationSize = waitingLocalMutations.size();
                //propagate info
                propagateSaturationState(inQueueSize, waitingSize);

                totalIntExec += internalMessagesExecuted;
                //LOG stuff
                StringBuilder sb = new StringBuilder();
                sb.append("\nInQ: ").append(inQueueSize).append("\tLMQ: ").append(localMutationSize)
                        .append("\tWaitQ: ").append(waitingSize).append("\tExecMut: ").append(MessageExecutorV3.currentMutations.size());

                sb.append("\nInRec: ").append(clientMessagesReceived).append("\tInExec: ").append(clientMessagesExecuted)
                        .append("\tLMQStored: ").append(clientMessagesStored).append("\tLMQExec: ").append(localMutationsExecuted)
                        .append("\nIntRec: ").append(internalMessagesReceived).append("\tIntExec: ").append(internalMessagesExecuted)
                        .append("(").append(totalIntExec).append(")")
                        .append("\tLoops: ").append(executorLoops).append("\tMutComplet: ").append(mutationsCompleted).append("(")
                        .append(totalMutations).append(")");
                sb.append("\n").append("clock: ").append(clock.toString());

                sb.append("\n");
                MessageExecutorV3.waitingMessages.forEach((k, v) ->
                        sb.append(k).append(": ").append(v.size()).append("\t"));

                if (inQueueSize > 100000 || waitingSize > 100000) {
                    logger.error(sb.toString());
                } else if (inQueueSize > 20000 || waitingSize > 20000) {
                    logger.warn(sb.toString());
                } else {
                    logger.info(sb.toString());
                }

                internalMessagesReceived = 0;
                clientMessagesReceived = 0;

                executorLoops = 0;
                clientMessagesExecuted = 0;
                clientMessagesStored = 0;
                localMutationsExecuted = 0;
                internalMessagesExecuted = 0;
                mutationsCompleted = 0;

            }
        }
    }

    private void handleNoDepMessage(Message msg) {
        if (msg.getCode() == UpMessage.CODE) {
            UpMessage m = (UpMessage) msg;
            logger.debug("Up message received: " + m);
            handleUpMessage(m);
        } else if (msg.getCode() == AckMessage.CODE) {
            AckMessage m = (AckMessage) msg;
            logger.debug("Ack message received " + m);
            handleAckMessage(m);
        } else if (msg.getCode() == StatusMessage.CODE) {
            StatusMessage m = (StatusMessage) msg;
            logger.debug("Status message received");
            handleStatusMessage(m);
        } else if (msg.getCode() == MigrateMessage.CODE) {
            MigrateMessage m = (MigrateMessage) msg;
            logger.debug("Migrate message received " + m);
            handleMigrateMessage(m);
        } else {
            logger.error("Unknown message received in receiver: " + msg);
        }
    }

    private void handleUpMessage(UpMessage msg) {
        String localDc = TreeManager.getInstance().getLocalDc();
        Map<String, Integer> opClock = cloneMap(clock);
        if (msg.getVerb() == Message.VERB_WRITE) {
            mutationClock++;
            opClock.put(localDc, mutationClock);
            logger.debug("New mutation clock: " + mutationClock);
        }
        InternalMessage internalMessage =
                new InternalMessage(msg.getTargets(), localDc, 0, opClock, msg.getFrom(), msg.getVerb(), msg.getTimestamp());

        if (internalMessage.getVerb() == Message.VERB_READ_REQUEST || internalMessage.getVerb() == Message.VERB_READ_REPLY) {
            //if read, propagate
            assert !internalMessage.getTargets().containsKey(localDc);
            MessageExecutorV3.propagateInternalMessage(internalMessage);
        } else {
            //if write, add to local mutations queue
            logger.debug("Storing in waitingLocalMutations");
            clientMessagesStored++;
            waitingLocalMutations.add(internalMessage);
        }

    }

    private void handleMigrateMessage(MigrateMessage m) {
        TreeManager tree = TreeManager.getInstance();

        Map<String, Integer> opClock = m.getClock();
        List<String> possibleDcs = m.getPossibleDatacenters();

        assert m.getSourceDc().equals(tree.getLocalDc());
        assert opClock == null || opClock.isEmpty();
        String bestDc = null;
        int bestDcLoad = Integer.MAX_VALUE;
        for (String dc : possibleDcs) {
            Pair<Integer, Long> pair = internalsStatus.get(dc);
            if (pair == null)
                continue;
            int load = pair.left;
            if (load < bestDcLoad) {
                bestDcLoad = load;
                bestDc = dc;
            }
        }
        possibleDcs = new LinkedList<>();
        possibleDcs.add(bestDc);
        opClock = cloneMap(clock);

        MigrateMessage newMsg = new MigrateMessage(m.getThread(), m.getSourceDc(), possibleDcs,
                opClock, m.getFrom(), m.getVerb(), m.getTimestamp());
        assert newMsg.getPossibleDatacenters().size() == 1;

        MessageExecutorV3.executeMigrateMessage(newMsg);
    }

    private void handleStatusMessage(StatusMessage sm) {
        for (Map.Entry<String, Pair<Integer, Long>> entry : sm.getStatus().entrySet()) {
            String dc = entry.getKey();
            Pair<Integer, Long> remotePair = entry.getValue();
            Pair<Integer, Long> localPair = internalsStatus.get(dc);
            if (localPair == null || remotePair.right > localPair.right) {
                internalsStatus.put(dc, remotePair);
            }
        }
    }

    private void handleAckMessage(AckMessage msg) {
        WaitingState waitingState = MessageExecutorV3.currentMutations.get(msg.getFrom());
        if (waitingState != null) {
            waitingState.receiveAck(msg.getNode(), msg.getId());
            if (waitingState.isCompleted()) {
                String sourceDc = waitingState.getSourceDc();
                logger.debug("Mutation completed " + sourceDc + " " + waitingState.getClock().get(sourceDc));
                MessageExecutorV3.currentMutations.remove(msg.getFrom());
                MessageExecutorV3.propagateInternalMessage(waitingState.getMessage());
                updateClock(sourceDc, waitingState.getClock().get(sourceDc));
                mutationsCompleted++;
                totalMutations++;
            }
        }
    }

    static void updateClock(String sourceDc, int val) {
        synchronized (clock) {
            int current = clock.getOrDefault(sourceDc, 0);
            assert val == current + 1;
            clock.put(sourceDc, val);
            logger.debug("New clock: " + clock);
            if (sourceDc.equals(TreeManager.getInstance().getLocalDc())) {
                localClockIncremented = true;
            } else {
                MessageExecutorV3.signalLoop();
            }
        }
    }

    private Map<String, Integer> cloneMap(Map<String, Integer> map) {
        return new HashMap<>(map);
    }

    private void propagateSaturationState(int inQueueSize, int waitingSize) {
        TreeManager tree = TreeManager.getInstance();

        long time = System.currentTimeMillis();
        internalsStatus.put(tree.getLocalDc(), Pair.create(inQueueSize + waitingSize, time));
        StatusMessage sm = new StatusMessage(time, tree.getLocalDc(), new HashMap<>(internalsStatus));

        List<String> targets = new LinkedList<>();
        targets.addAll(tree.getDownBranches(tree.getMainTree()));
        String inverse = tree.getUpBranch(tree.getMainTree());
        if (inverse != null)
            targets.add(inverse);

        for (String dc : targets) {
            try {
                InetAddress dcAddress = InetAddress.getByName(tree.getDcAddress(dc));
                ConnectionManager.getConnectionToHigh(dcAddress).writeAndFlush(sm);
            } catch (Exception e) {
                logger.error("Failed to send status message to " + dc + ". " + e.getMessage());
            }
        }

    }

    public static void addToNoDependencyQueue(Message msg) {
        clientMessagesReceived++;
        noDependencyQueue.add(msg);
    }

    @Override
    public void run() {
        try {
            mainLoop();
        } catch (Exception e) {
            logger.error("Exception in receiver thread: " + e);
            e.printStackTrace();
            System.exit(1);
        }
    }

}
