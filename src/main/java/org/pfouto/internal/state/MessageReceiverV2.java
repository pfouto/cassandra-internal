package org.pfouto.internal.state;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pfouto.internal.connection.ConnectionManager;
import org.pfouto.internal.messaging.*;
import org.pfouto.internal.tree.TreeManager;
import org.pfouto.internal.utils.Pair;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;
import java.util.concurrent.*;

public class MessageReceiverV2 implements Runnable {

    private static final Logger logger = LogManager.getLogger(MessageReceiverV2.class);

    public static BlockingQueue<Message> messageInQueue = new LinkedBlockingQueue<>();

    private int mutationClock;
    protected static Map<String, Integer> clock;

    private long lastStatus;
    private static final long STATUS_INTERVAL = 1000;

    private Map<String, Pair<Integer, Long>> internalsStatus;

    //logs
    protected static int executorLoops;
    protected static int messagesReceived;
    protected static int messagesStored;
    protected static int messagesExecutedByReceiver;
    protected static int messagesExecutedByExecutor;

    public MessageReceiverV2() {
        mutationClock = 0;
        clock = new ConcurrentHashMap<>();
        internalsStatus = new HashMap<>();
        lastStatus = System.currentTimeMillis();

        messagesExecutedByExecutor = 0;
        messagesExecutedByReceiver = 0;
        messagesReceived = 0;
        messagesStored = 0;
        executorLoops = 0;
    }

    private void mainLoop() throws InterruptedException {
        //noinspection InfiniteLoopStatement
        while (true) {

            Message in = messageInQueue.poll(1000, TimeUnit.MILLISECONDS);
            if (in != null) {
                handleMessage(in);
                messagesReceived++;
            }

            long now = System.currentTimeMillis();
            if (now - lastStatus > STATUS_INTERVAL) {

                lastStatus = now;
                int inQueueSize = messageInQueue.size();
                int waitingSize = MessageExecutorV2.getWaitingMessagesSize();
                //propagate info
                propagateSaturationState(inQueueSize, waitingSize);

                //LOG stuff
                StringBuilder sb = new StringBuilder();
                sb.append("\nInQ: ").append(inQueueSize).append("\tWaitQ: ").append(waitingSize)
                        .append("\tLocalQueue: ").append(MessageExecutorV2.getWaitingMessagesLocalSize());
                sb.append("\nRec: ").append(messagesReceived).append("\tExRec: ").append(messagesExecutedByReceiver)
                        .append("\tStored: ").append(messagesStored).append("\tExExec: ").append(messagesExecutedByExecutor)
                        .append("\tLoops: ").append(executorLoops);

                sb.append("\n");
                sb.append("Waiting: ");
                MessageExecutorV2.waitingMessages.forEach((k, v) ->
                        sb.append(k).append(": ").append(v.size()).append("\t"));

                sb.append("\n");
                sb.append("Clock: ").append(clock);
                if (inQueueSize > 100000 || waitingSize > 100000) {
                    logger.error(sb.toString());
                } else if (inQueueSize > 20000 || waitingSize > 20000){
                    logger.warn(sb.toString());
                } else {
                    logger.info(sb.toString());
                }

                messagesExecutedByExecutor = 0;
                messagesExecutedByReceiver = 0;
                messagesReceived = 0;
                messagesStored = 0;
                executorLoops = 0;
            }

        }
    }

    private void handleMessage(Message msg) {
        if (msg.getCode() == UpMessage.CODE) {
            UpMessage m = (UpMessage) msg;
            logger.debug("Up message received: " + m);
            handleUpMessage(m);
        } else if (msg.getCode() == InternalMessage.CODE) {
            InternalMessage m = (InternalMessage) msg;
            logger.debug("Internal message received: " + m);
            handleInternalMessage(m);
        } else if (msg.getCode() == ClockMessage.CODE) {
            ClockMessage m = (ClockMessage) msg;
            logger.debug("Clock message received " + m);
            handleClockMessage(m);
        } else if (msg.getCode() == AckMessage.CODE) {
            AckMessage m = (AckMessage) msg;
            logger.debug("Ack message received " + m);
            handleAckMessage(m);
        } else if (msg.getCode() == StatusMessage.CODE) {
            StatusMessage m = (StatusMessage) msg;
            logger.debug("Status message received " + m);
            handleStatusMessage(m);
        } else if (msg.getCode() == MigrateMessage.CODE) {
            MigrateMessage m = (MigrateMessage) msg;
            logger.debug("Migrate message received " + m);
            handleMigrateMessage(m);
        } else {
            logger.error("Unknown message received in receiver: " + msg);
        }
    }

    private void propagateSaturationState(int inQueueSize, int waitingSize) {
        TreeManager tree = TreeManager.getInstance();

        long time = System.currentTimeMillis();
        internalsStatus.put(tree.getLocalDc(), Pair.create(inQueueSize + waitingSize, time));
        StatusMessage sm = new StatusMessage(time, tree.getLocalDc(), new HashMap<>(internalsStatus));

        List<String> targets = new LinkedList<>();
        targets.addAll(tree.getDownBranches(tree.getMainTree()));
        String inverse = tree.getUpBranch(tree.getMainTree());
        if (inverse != null)
            targets.add(inverse);

        for (String dc : targets) {
            try {
                InetAddress dcAddress = InetAddress.getByName(tree.getDcAddress(dc));
                ConnectionManager.getConnectionToHigh(dcAddress).writeAndFlush(sm);
            } catch (Exception e) {
                logger.error("Failed to send status message to " + dc + ". " + e.getMessage());
            }
        }

    }

    private void handleMigrateMessage(MigrateMessage m) {
        TreeManager tree = TreeManager.getInstance();

        Map<String, Integer> opClock = m.getClock();
        List<String> possibleDcs = m.getPossibleDatacenters();

        //if from client
        if (possibleDcs.size() > 1) {
            assert m.getSourceDc().equals(tree.getLocalDc());
            assert opClock == null || opClock.isEmpty();
            String bestDc = null;
            int bestDcLoad = Integer.MAX_VALUE;
            for (String dc : possibleDcs) {
                Pair<Integer, Long> pair = internalsStatus.get(dc);
                if (pair == null)
                    continue;
                int load = pair.left;
                if (load < bestDcLoad) {
                    bestDcLoad = load;
                    bestDc = dc;
                }
            }
            possibleDcs = new LinkedList<>();
            possibleDcs.add(bestDc);
            opClock = cloneMap(clock);
        }

        MigrateMessage newMsg = new MigrateMessage(m.getThread(), m.getSourceDc(), possibleDcs,
                opClock, m.getFrom(), m.getVerb(), m.getTimestamp());
        assert newMsg.getPossibleDatacenters().size() == 1;

        if (newMsg.getPossibleDatacenters().get(0).equals(tree.getLocalDc())) {
            //target is me
            MessageExecutorV2.addToWaitingMessages(newMsg.getSourceDc(), newMsg);
        } else {
            messagesExecutedByReceiver++;
            //or not
            for (String dc : tree.getDownBranches(newMsg.getSourceDc())) {
                Set<String> targets = tree.getTargetsBehindBranch(newMsg.getSourceDc(), dc);
                if (targets.contains(newMsg.getPossibleDatacenters().get(0))) {
                    logger.debug("Redirecting migrate to: " + dc);
                    try {
                        InetAddress dcAddress = InetAddress.getByName(tree.getDcAddress(dc));
                        ConnectionManager.getConnectionToHigh(dcAddress).writeAndFlush(newMsg);
                    } catch (Exception e) {
                        logger.error("Failed to migration message to " + dc + ". " + e.getMessage());
                    }
                    break;
                }
            }
        }
    }

    private void handleStatusMessage(StatusMessage sm) {
        messagesExecutedByReceiver++;
        for (Map.Entry<String, Pair<Integer, Long>> entry : sm.getStatus().entrySet()) {
            String dc = entry.getKey();
            Pair<Integer, Long> remotePair = entry.getValue();
            Pair<Integer, Long> localPair = internalsStatus.get(dc);
            if (localPair == null || remotePair.right > localPair.right) {
                internalsStatus.put(dc, remotePair);
            }
        }
    }

    private void handleClockMessage(ClockMessage msg) {
        //propagate
        for (String dc : TreeManager.getInstance().getDownBranches(msg.getClock().left)) {
            try {
                InetAddress dcAddress = InetAddress.getByName(TreeManager.getInstance().getDcAddress(dc));
                ConnectionManager.getConnectionToHigh(dcAddress).writeAndFlush(msg);
            } catch (UnknownHostException | InterruptedException e) {
                logger.error("Failed to forward clock to " + dc + ". " + e.getMessage());
            }
        }
        MessageExecutorV2.addToWaitingMessages(msg.getClock().left, msg);
    }

    private void handleUpMessage(UpMessage msg) {
        String localDc = TreeManager.getInstance().getLocalDc();

        Map<String, Integer> opClock = cloneMap(clock);

        if (msg.getVerb() == Message.VERB_WRITE) {
            mutationClock++;
            opClock.put(localDc, mutationClock);
            logger.debug("New mutation clock: " + mutationClock);
        }

        InternalMessage internalMessage =
                new InternalMessage(msg.getTargets(), localDc, 0, opClock, msg.getFrom(), msg.getVerb(), msg.getTimestamp());
        handleInternalMessage(internalMessage);
    }

    private void handleInternalMessage(InternalMessage msg) {
        TreeManager tree = TreeManager.getInstance();
        ClockMessage cm = null;

        //propagate
        for (String dc : tree.getDownBranches(msg.getSourceDc())) {

            //Find targets in that branch
            Set<String> targets = tree.getTargetsBehindBranch(msg.getSourceDc(), dc);

            targets.retainAll(msg.getTargets().keySet());

            //Decide message or clock (or none) and create
            Message m = null;
            if (!targets.isEmpty()) {
                m = msg.filterAndClone(targets);
                logger.debug("Redirecting to: " + dc);
            } else if (msg.getVerb() == Message.VERB_WRITE) {
                if (cm == null) cm = ClockMessage.fromInternal(msg);
                logger.debug("Clock to: " + dc);
                m = cm;
            }

            //Send
            if (m != null) {
                try {
                    InetAddress dcAddress = InetAddress.getByName(tree.getDcAddress(dc));
                    ConnectionManager.getConnectionToHigh(dcAddress).writeAndFlush(m);
                } catch (Exception e) {
                    logger.error("Failed to forward message to " + dc + ". " + e.getMessage());
                }
            }
        }

        //if read not for me, nothing to do here
        if ((msg.getVerb() == Message.VERB_READ_REQUEST || msg.getVerb() == Message.VERB_READ_REPLY)
                && !msg.getTargets().containsKey(tree.getLocalDc())) {
            messagesExecutedByReceiver++;
        } else {
            //if read of me or mutate
            MessageExecutorV2.addToWaitingMessages(msg.getSourceDc(), msg);
        }
    }

    private void handleAckMessage(AckMessage msg) {
        WaitingState waitingState = MessageExecutorV2.currentMutations.get(msg.getFrom());
        if (waitingState != null) {
            waitingState.receiveAck(msg.getNode(), msg.getId());
            if (waitingState.isCompleted()) {
                logger.debug("Mutation completed");
                String sourceDc = waitingState.getSourceDc();
                assert sourceDc != null;
                Map<String, Integer> clock = waitingState.getClock();
                assert clock != null;
                Integer integer = clock.get(sourceDc);
                assert integer != null;
                MessageExecutorV2.currentMutations.remove(msg.getFrom());
                updateClock(sourceDc, integer);
            }
        }
        messagesExecutedByReceiver++;
    }

    private void updateClock(String sourceDc, int val) {
        int current = clock.getOrDefault(sourceDc, 0);
        assert val == current + 1;
        if (current < val) {
            clock.put(sourceDc, val);
            logger.debug("New clock: " + clock);
        } else {
            assert false;
        }
        MessageExecutorV2.signalLoop();
    }

    private Map<String, Integer> cloneMap(Map<String, Integer> map) {
        return new HashMap<>(map);
    }

    @Override
    public void run() {
        try {
            mainLoop();
        } catch (Exception e) {
            logger.error("Exception in receiver thread: " + e);
            e.printStackTrace();
            System.exit(1);
        }
    }
}
